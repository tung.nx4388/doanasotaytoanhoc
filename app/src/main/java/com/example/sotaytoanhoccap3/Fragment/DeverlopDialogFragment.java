package com.example.sotaytoanhoccap3.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;

public class DeverlopDialogFragment extends DialogFragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_deverlop,container,false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            TextView txt_title = view.findViewById(R.id.txt_TitleDeverlop);
            txt_title.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_admin1 = view.findViewById(R.id.txt_admin1);
            txt_admin1.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_admin2 = view.findViewById(R.id.txt_admin2);
            txt_admin2.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }
}
