package com.example.sotaytoanhoccap3.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Activity.WelcomeActivity;
import com.example.sotaytoanhoccap3.Adapter.BackgroundColorAdapter;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticImageView;

public class SetColorBackgroundDialogFragment extends DialogFragment implements View.OnClickListener{
    private InformationData informationData;
    private View v;

    GridView gridView;
    Button btCustom,bt_Oke,bt_Cancel;
    LinearLayout linearCustom;
    public static FrameLayout framePreview;
    Boolean checkLinear;
    EditText edt_choice;
    Button bt_choice;
    static String color;
    static String colorhint;

    static TextView txt_preview,txt_TitlePreview;
    static ElasticImageView imgPreview;
    static EditText edt_fieldemailpreview,edt_fieldpasspreview;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_backgroundcolor,container,false);

        gridView = v.findViewById(R.id.gridViewBackground);
        btCustom = v.findViewById(R.id.cardViewCustom);
        linearCustom = v.findViewById(R.id.linearCustom);
        linearCustom.setVisibility(View.GONE);
        framePreview = v.findViewById(R.id.framelayout_preview);
        framePreview.setVisibility(View.GONE);
        bt_Cancel = v.findViewById(R.id.bt_cancelbackgroundcolor);
        bt_Oke = v.findViewById(R.id.bt_okchangebackground);
        edt_choice = v.findViewById(R.id.edt_choicecolor);
        bt_choice = v.findViewById(R.id.bt_choicecolor);

        txt_preview = v.findViewById(R.id.txt_preview);
        txt_TitlePreview = v.findViewById(R.id.txtTitlePreview);
        imgPreview = v.findViewById(R.id.imgPreview);
        edt_fieldemailpreview = v.findViewById(R.id.fieldEmailPreview);
        edt_fieldpasspreview = v.findViewById(R.id.fieldPasswordPreview);

       checkLinear = false;
        btCustom.setOnClickListener(this);
        bt_Oke.setOnClickListener(this);
        bt_Cancel.setOnClickListener(this);
        bt_choice.setOnClickListener(this);

        informationData = new InformationData(getContext());

        String colores[] = {"#EF4444","#FAA31B","#FFF000","#82C341","#009F75","#88C6ED","#394BA0","#D54799"};
        BackgroundColorAdapter backgroundColorAdapter = new BackgroundColorAdapter(colores);
        gridView.setAdapter(backgroundColorAdapter);

        return v;
    }

    public static void setBackgroundPreview(String color)
    {
        try {
            txt_preview.setTextColor(Color.parseColor(color));
            txt_TitlePreview.setTextColor(Color.parseColor(color));
            imgPreview.setColorFilter(Color.parseColor(color));
            edt_fieldemailpreview.setTextColor(Color.parseColor(color));
            edt_fieldpasspreview.setTextColor(Color.parseColor(color));
            String colorhint = "#FFFFFFFF";
            if(color.length()==7)
            {
                colorhint = color.replace("#","#9A");
            }
            else {
                colorhint = "#9A"+color.substring(3);
            }
            edt_fieldemailpreview.setHintTextColor(Color.parseColor(colorhint));
            edt_fieldpasspreview.setHintTextColor(Color.parseColor(colorhint));
            SetColorBackgroundDialogFragment.color = color;
            SetColorBackgroundDialogFragment.colorhint = colorhint;
        }
        catch (Exception ex)
        {
            Toast.makeText(MainActivity.activity, "Mã màu không tồn tại", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.cardViewCustom:{
                if(checkLinear)
                {
                    linearCustom.setVisibility(View.GONE);
                    checkLinear = !checkLinear;
                }
                else {
                    linearCustom.setVisibility(View.VISIBLE);
                    checkLinear = !checkLinear;
                }
            }break;
            case R.id.bt_okchangebackground:{
                if(SetColorBackgroundDialogFragment.color == null)
                {
                    Toast.makeText(getContext(), "Chưa chọn màu", Toast.LENGTH_SHORT).show();
                }
                else {
                    informationData.putKey(GetStringKey.color, SetColorBackgroundDialogFragment.color);
                    informationData.putKey(GetStringKey.colorhint, SetColorBackgroundDialogFragment.colorhint);
                    SetColorBackgroundDialogFragment.color = null;
                    Toast.makeText(getContext(), "Xin đợi reset lại ứng dụng", Toast.LENGTH_SHORT).show();
                    getActivity().startActivity(new Intent(getActivity(), WelcomeActivity.class));
                    getActivity().finish();
                    dismiss();
                }
            }break;
            case R.id.bt_cancelbackgroundcolor:{
                dismiss();
            }break;
            case R.id.bt_choicecolor:{
                if(edt_choice.getText().toString().length() >= 6)
                {
                    framePreview.setVisibility(View.VISIBLE);
                    setBackgroundPreview("#"+edt_choice.getText().toString());
                }
                else {
                    Toast.makeText(getContext(), "Vui lòng nhập tối thiểu 6 ký tự mã màu", Toast.LENGTH_SHORT).show();
                }
            }break;
        }
    }
    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            TextView textView = v.findViewById(R.id.txt_TitleSetBackgroundFragment);
            textView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            btCustom.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_Oke.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_Cancel.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_choice.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_choice.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_choice.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            txt_preview.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            txt_TitlePreview.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            imgPreview.setColorFilter(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_fieldemailpreview.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_fieldpasspreview.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_fieldemailpreview.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            edt_fieldpasspreview.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }
}
