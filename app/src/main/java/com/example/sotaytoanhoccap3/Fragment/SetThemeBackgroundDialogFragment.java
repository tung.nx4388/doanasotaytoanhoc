package com.example.sotaytoanhoccap3.Fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.sotaytoanhoccap3.Activity.WelcomeActivity;
import com.example.sotaytoanhoccap3.Adapter.BackgroundThemeStorageAdapter;
import com.example.sotaytoanhoccap3.Adapter.ViewPagerThemeBackgroundAdapter;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;

import java.util.ArrayList;

public class SetThemeBackgroundDialogFragment extends DialogFragment implements View.OnClickListener {
    private View view;
    ViewPager viewPager;
    Button bt_ok,bt_cancel,bt_custom;
    public static Integer resImagechoice;
    ProgressBar progressBar;
    InformationData informationData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_backgroundtheme,container,false);
        viewPager = view.findViewById(R.id.viewPagerThemBackground);
        bt_ok = view.findViewById(R.id.bt_okchangebackgroundtheme);
        bt_cancel = view.findViewById(R.id.bt_cancelbackgroundtheme);
        bt_custom = view.findViewById(R.id.bt_Custom);
        progressBar = view.findViewById(R.id.progress_circular);

        bt_cancel.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
        bt_custom.setOnClickListener(this);

        informationData = new InformationData(getContext());
        informationData.putKey(GetStringKey.choicetheme,"0");

        ArrayList<ThemeItemFragment> themeItemFragmentArrayList = new ArrayList<>();
        ThemeItemFragment themeItemFragment1 = new ThemeItemFragment();
        themeItemFragment1.selectThemeItemFragment(1);
        ThemeItemFragment themeItemFragment2 = new ThemeItemFragment();
        themeItemFragment2.selectThemeItemFragment(2);
        themeItemFragmentArrayList.add(themeItemFragment1);
        themeItemFragmentArrayList.add(themeItemFragment2);
        ViewPagerThemeBackgroundAdapter viewPagerThemeBackgroundAdapter = new ViewPagerThemeBackgroundAdapter(getChildFragmentManager(),themeItemFragmentArrayList);
        viewPager.setAdapter(viewPagerThemeBackgroundAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position)
                {
                    case 0:{
                        bt_custom.setText("Tùy chỉnh");
                        informationData.putKey(GetStringKey.choicetheme,"0");
                    }break;
                    case 1:{
                        bt_custom.setText("Mặc định");
                        informationData.putKey(GetStringKey.choicetheme,"1");
                    }break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }

    private void setBackgroundColor() {
        InformationData informationData = new InformationData(getContext());
        if (informationData.getKey(GetStringKey.color).compareTo("") != 0) {
            TextView txt_TitleSetBackgroundFragment = view.findViewById(R.id.txt_TitleSetBackgroundFragment);
            txt_TitleSetBackgroundFragment.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_ok.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_cancel.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_custom.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_okchangebackgroundtheme:{
                if(resImagechoice != null &&  informationData.getKey(GetStringKey.choicetheme).compareTo("0")==0)
                {
                    showProgressBar();
                    InformationData informationData = new InformationData(getContext());
                    informationData.putKey(GetStringKey.themenormal,resImagechoice.toString());
                    resImagechoice = null;
                    Toast.makeText(getContext(), "Xin đợi reset lại ứng dụng", Toast.LENGTH_SHORT).show();
                    getActivity().startActivity(new Intent(getActivity(), WelcomeActivity.class));
                    getActivity().finish();
                    hideProgressBar();
                    dismiss();
                }
                else if(informationData.getKey(GetStringKey.choicetheme).compareTo("1")==0 && BackgroundThemeStorageAdapter.abc != null)
                {
                    showProgressBar();
                    Glide.with(getContext())
                            .load(BackgroundThemeStorageAdapter.abc)
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    informationData.putBitmap(resource, GetStringKey.themebitmap);
                                }
                            });
                    BackgroundThemeStorageAdapter.abc = null;
                    Toast.makeText(getContext(), "Xin đợi reset lại ứng dụng", Toast.LENGTH_SHORT).show();
                    getActivity().startActivity(new Intent(getActivity(), WelcomeActivity.class));
                    getActivity().finish();
                    hideProgressBar();
                    dismiss();
                }
                else {
                    Toast.makeText(getContext(), "Chưa chọn hình nền", Toast.LENGTH_SHORT).show();
                }
            }break;
            case R.id.bt_cancelbackgroundtheme:{
                resImagechoice = null;
                dismiss();
            }break;
            case R.id.bt_Custom:{
                if(bt_custom.getText().toString().compareTo("Tùy chỉnh")==0)
                {
                    bt_custom.setText("Mặc định");
                    informationData.putKey(GetStringKey.choicetheme,"1");
                    viewPager.setCurrentItem(1);
                }
                else {
                    bt_custom.setText("Tùy chỉnh");
                    informationData.putKey(GetStringKey.choicetheme,"0");
                    viewPager.setCurrentItem(0);
                }
            }break;
        }
    }

    private void showProgressBar()
    {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar()
    {
        progressBar.setVisibility(View.INVISIBLE);
    }
}
