package com.example.sotaytoanhoccap3.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Interface.CheckUpdateVersion;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;

public class UpdateCheckFragment extends DialogFragment implements View.OnClickListener{
    CheckUpdateVersion checkUpdateVersion;
    Button bt_yes;
    Button bt_no;
    private View view;

    @Override
    public void onAttach(@NonNull Context context) {
        try {
            checkUpdateVersion = (CheckUpdateVersion) context;
        }catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString());
        }
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkupdate,container,false);
        bt_yes = view.findViewById(R.id.bt_YES);
        bt_no = view.findViewById(R.id.bt_NO);

        bt_yes.setOnClickListener(this);
        bt_no.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_YES:{
                checkUpdateVersion.setUpdate(true);
            }break;
            case R.id.bt_NO:{
                checkUpdateVersion.setUpdate(false);
            }break;
        }
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            TextView txt_TitleUpdate = view.findViewById(R.id.txt_TitleUpdate);
            txt_TitleUpdate.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_InformationUpdate = view.findViewById(R.id.txt_InformationUpdate);
            txt_InformationUpdate.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_yes.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_no.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }
}
