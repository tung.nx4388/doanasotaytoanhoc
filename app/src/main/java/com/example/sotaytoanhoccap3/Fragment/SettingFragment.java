package com.example.sotaytoanhoccap3.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticCardView;

public class SettingFragment extends Fragment implements View.OnClickListener{
    private String title;
    private int page;
    ElasticCardView cardView_colorbackground,cardView_themebackground,cardViewDevelopers;
    DialogFragment dialogFragment;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting,container,false);
        cardView_colorbackground = view.findViewById(R.id.cardview_colorbackground);
        cardView_themebackground = view.findViewById(R.id.cardview_themebackground);
        cardViewDevelopers = view.findViewById(R.id.cardViewDevelopers);

        cardView_colorbackground.setOnClickListener(this);
        cardView_themebackground.setOnClickListener(this);
        cardViewDevelopers.setOnClickListener(this);

//        InformationData informationData = new InformationData(getContext());
//        String colores[] = {"#EF4444","#FAA31B","#FFF000","#82C341","#009F75","#88C6ED","#394BA0","#D54799"};

        return  view;
    }

    public static SettingFragment newInstance(int page,String title) {

        Bundle args = new Bundle();

        SettingFragment fragment = new SettingFragment();
        args.putInt("someInt",page);
        args.putString("someTitle",title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            TextView txt_title = view.findViewById(R.id.txt_TitleSetting);
            txt_title.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_colorsetting = view.findViewById(R.id.txt_changecolorsetting);
            txt_colorsetting.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_background = view.findViewById(R.id.txt_changebackground);
            txt_background.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView txt_admin = view.findViewById(R.id.txt_informationamdin);
            txt_admin.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.cardview_colorbackground:{
                dialogFragment = new SetColorBackgroundDialogFragment();
                dialogFragment.show(getActivity().getSupportFragmentManager(),null);
            }break;
            case R.id.cardview_themebackground:{
                dialogFragment = new SetThemeBackgroundDialogFragment();
                dialogFragment.show(getActivity().getSupportFragmentManager(),null);
            }break;
            case R.id.cardViewDevelopers:{
                dialogFragment = new DeverlopDialogFragment();
                dialogFragment.show(getActivity().getSupportFragmentManager(),null);
            }break;
        }
    }
}
