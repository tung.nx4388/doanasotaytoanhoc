package com.example.sotaytoanhoccap3.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sotaytoanhoccap3.Adapter.FavoriteAdapter;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;

import java.util.ArrayList;

public class FavoriteFragment extends Fragment {
    static ListView listView;
    static ArrayList<ItemInformation> itemInformationsFavorite;

    private String title;
    private int page;

    static View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite,container,false);

        listView = view.findViewById(R.id.listView);

        Install(getContext());

        return view;
    }

    public static void Install(Context context)
    {
        itemInformationsFavorite = GeneralData.getListItemFavorite(context);

        if(itemInformationsFavorite.size() == 0)
        {
            itemInformationsFavorite = new ArrayList<>();
            itemInformationsFavorite.add(new ItemInformation("Chưa có dữ liệu",false,null,""));
        }
        FavoriteAdapter favoriteAdapter = new FavoriteAdapter(itemInformationsFavorite);
        if(listView != null)
        listView.setAdapter(favoriteAdapter);
    }

    public static FavoriteFragment newInstance(int page,String title) {

        Bundle args = new Bundle();

        FavoriteFragment fragment = new FavoriteFragment();
        args.putInt("someInt",page);
        args.putString("someTitle",title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            TextView textView = view.findViewById(R.id.txt_TitleFavorite);
            textView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setBackgroundColor();
    }
}
