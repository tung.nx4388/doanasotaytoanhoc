package com.example.sotaytoanhoccap3.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sotaytoanhoccap3.Adapter.BackgroundThemeStorageAdapter;
import com.example.sotaytoanhoccap3.Adapter.BackgroundThemenormalAdapter;
import com.example.sotaytoanhoccap3.R;

import java.util.ArrayList;

public class ThemeItemFragment extends Fragment {
    View view;
    GridView gridView;
    int select;

    public void selectThemeItemFragment(int select) {
        this.select = select;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_itemgridview,container,false);
        gridView = view.findViewById(R.id.griviewBackroundTheme);

        switch (select)
        {
            case 1:{
                ArrayList<Integer> arrayList = new ArrayList<>();
                arrayList.add(R.drawable.background1);
                arrayList.add(R.drawable.background2);
                arrayList.add(R.drawable.background3);
                arrayList.add(R.drawable.background4);
                BackgroundThemenormalAdapter backgroundThemenormalAdapter = new BackgroundThemenormalAdapter(arrayList);
                gridView.setAdapter(backgroundThemenormalAdapter);
            }break;
            case 2:{
                BackgroundThemeStorageAdapter backgroundThemeStorageAdapter = new BackgroundThemeStorageAdapter(getActivity());
                gridView.setAdapter(backgroundThemeStorageAdapter);
            }break;
        }

        return view;
    }
}
