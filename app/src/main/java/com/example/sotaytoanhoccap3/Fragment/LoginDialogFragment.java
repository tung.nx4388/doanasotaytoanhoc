package com.example.sotaytoanhoccap3.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthMultiFactorException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.MultiFactorResolver;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

public class LoginDialogFragment extends DialogFragment implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    String TAG = "LOGIN";

    private TextView mStatusTextView;
    private TextView mDetailTextView;
    private TextView txt_forgotpass;
    private TextView txt_titleLogin;

    private EditText mEmailField;
    private EditText mPasswordField;
    private EditText edt_newpass;
    private EditText edt_confignewpass;

    private Button bt_signin,bt_createaccount, bt_signout,bt_veryfy,bt_reload,bt_changepass,bt_changepassOK,bt_changepasscancel;
    private CheckBox checkBoxShowPass;
    private ProgressBar progressBar;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login,container,false);

        // Views
        mStatusTextView = view.findViewById(R.id.status);
        mDetailTextView = view.findViewById(R.id.detail);
        mEmailField = view.findViewById(R.id.fieldEmail);
        mPasswordField = view.findViewById(R.id.fieldPassword);
        bt_createaccount = view.findViewById(R.id.emailCreateAccountButton);
        bt_signin = view.findViewById(R.id.emailSignInButton);
        bt_signout = view.findViewById(R.id.signOutButton);
        bt_reload = view.findViewById(R.id.reloadButton);
        bt_veryfy = view.findViewById(R.id.verifyEmailButton);
        checkBoxShowPass = view.findViewById(R.id.check_showpassword);
        progressBar = view.findViewById(R.id.progress_circular);
        bt_changepass = view.findViewById(R.id.bt_changePass);
        bt_changepassOK = view.findViewById(R.id.bt_changepassOK);
        txt_forgotpass = view.findViewById(R.id.txt_forgotpass);
        edt_confignewpass = view.findViewById(R.id.fieldconfignewPassword);
        edt_newpass = view.findViewById(R.id.fieldnewPassword);
        bt_changepasscancel = view.findViewById(R.id.bt_canchangepass);
        txt_titleLogin = view.findViewById(R.id.titleText);

        // Buttons
        bt_signin.setOnClickListener(this);
        bt_createaccount.setOnClickListener(this);
        bt_signout.setOnClickListener(this);
        bt_veryfy.setOnClickListener(this);
        bt_reload.setOnClickListener(this);
        checkBoxShowPass.setOnClickListener(this);
        bt_changepassOK.setOnClickListener(this);
        bt_changepass.setOnClickListener(this);
        txt_forgotpass.setOnClickListener(this);
        bt_changepasscancel.setOnClickListener(this);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        return view;
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            txt_titleLogin.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            mStatusTextView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            mDetailTextView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            mEmailField.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            mEmailField.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            mPasswordField.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            mPasswordField.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_newpass.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_newpass.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            edt_confignewpass.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            edt_confignewpass.setHintTextColor(Color.parseColor(informationData.getKey(GetStringKey.colorhint)));
            checkBoxShowPass.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            txt_forgotpass.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_changepassOK.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_changepasscancel.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_signin.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_createaccount.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_signout.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_changepass.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_veryfy.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            bt_reload.setBackgroundColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        if(getDialog()==null)
            return;
        else {
            getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
        }
        setBackgroundColor();
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateFormLogin()) {
            return;
        }

        showProgressBar();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            GetDataFirebase.CretateUID(user,db,getContext());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Đăng kí thất bại",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        hideProgressBar();

                    }
                });
        // [END create_user_with_email]
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateFormLogin()) {
            return;
        }

        showProgressBar();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            GetDataFirebase.UpdateRecentlyFavoriteSignin(user,db,getContext());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Đăng nhập thất bại",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                            // [START_EXCLUDE]
                            checkForMultiFactorFailure(task.getException());
                            // [END_EXCLUDE]
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                            mStatusTextView.setText(GetStringKey.auth_failed);
                        }

                        hideProgressBar();

                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private void updatepass(final FirebaseUser user, String oldpass, final String newPass) {
        String email = user.getEmail();

        showProgressBar();
        AuthCredential credential = EmailAuthProvider.getCredential(email,oldpass);

        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getContext(), "Đổi mật khẩu thành công, xin vui lòng đăng nhập lại", Toast.LENGTH_SHORT).show();
                                hideChangepass();
                                signOut();
                            }else {
                                Toast.makeText(getContext(), "Đổi mật khẩu thất bại", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else {
                    Toast.makeText(getContext(), "Kết nối thất bại", Toast.LENGTH_SHORT).show();
                }
                hideProgressBar();
            }
        });
    }

    private void signOut() {
        mAuth.signOut();

        updateUI(null);
    }

    private void sendEmailVerification() {
        // Disable button
        view.findViewById(R.id.verifyEmailButton).setEnabled(false);

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        // Re-enable button
                        bt_veryfy.setEnabled(true);

                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(),
                                    "Email xác nhận được gửi đến " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(getContext(),
                                    "Gửi email xác nhận thất bại",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
        // [END send_email_verification]
    }

    private void reload() {
        showProgressBar();
        mAuth.getCurrentUser().reload().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    updateUI(mAuth.getCurrentUser());
                    Toast.makeText(getContext(),
                            "Tải lại thành công",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Log.e(TAG, "reload", task.getException());
                    Toast.makeText(getContext(),
                            "Tải lại thất bại",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean validateFormLogin() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Còn trống");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Còn trống");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    private boolean validateFormChangePass() {
        boolean valid = true;

        String newpass = edt_newpass.getText().toString();
        if (TextUtils.isEmpty(newpass)) {
            edt_newpass.setError("Còn trống");
            valid = false;
        } else {
            edt_newpass.setError(null);
        }

        String confignewpass = edt_confignewpass.getText().toString();
        if (TextUtils.isEmpty(confignewpass)) {
            edt_confignewpass.setError("Còn trống");
            valid = false;
        } else {
            edt_confignewpass.setError(null);
        }

        if(newpass.compareTo(confignewpass)!=0)
        {
            Toast.makeText(getContext(), "Mật khẩu không trùng khớp", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

    private boolean validateFormRestPass() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Còn trống");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        hideProgressBar();
        if (user != null) {
            mStatusTextView.setText(String.format("Email User: %1$s (Xác nhận: %2$b)",
                    user.getEmail(), user.isEmailVerified()));
            mDetailTextView.setText(String.format("Firebase UID: %s", user.getUid()));

            view.findViewById(R.id.emailPasswordButtons).setVisibility(View.GONE);
            view.findViewById(R.id.fieldEmail).setVisibility(View.GONE);
            view.findViewById(R.id.fieldPassword).setVisibility(View.GONE);
            view.findViewById(R.id.signedInButtons).setVisibility(View.VISIBLE);
            txt_forgotpass.setVisibility(View.GONE);
            checkBoxShowPass.setVisibility(View.GONE);
            MainActivity.txt_accountloginnav.setText(user.getEmail());

            if (user.isEmailVerified()) {
                view.findViewById(R.id.verifyEmailButton).setVisibility(View.GONE);
            } else {
                view.findViewById(R.id.verifyEmailButton).setVisibility(View.VISIBLE);
            }
        } else {
            mStatusTextView.setText(null);
            mDetailTextView.setText(null);

            view.findViewById(R.id.emailPasswordButtons).setVisibility(View.VISIBLE);
            view.findViewById(R.id.fieldEmail).setVisibility(View.VISIBLE);
            view.findViewById(R.id.fieldPassword).setVisibility(View.VISIBLE);
            view.findViewById(R.id.signedInButtons).setVisibility(View.GONE);
            txt_forgotpass.setVisibility(View.VISIBLE);
            checkBoxShowPass.setVisibility(View.VISIBLE);
            MainActivity.txt_accountloginnav.setText("No user");
        }
        view.findViewById(R.id.lin_changepass).setVisibility(View.GONE);
        view.findViewById(R.id.constrain).setVisibility(View.GONE);
    }

    private void checkForMultiFactorFailure(Exception e) {
        // Multi-factor authentication with SMS is currently only available for
        // Google Cloud Identity Platform projects. For more information:
        // https://cloud.google.com/identity-platform/docs/android/mfa
        if (e instanceof FirebaseAuthMultiFactorException) {
            Log.w(TAG, "multiFactorFailure", e);
            Intent intent = new Intent();
            MultiFactorResolver resolver = ((FirebaseAuthMultiFactorException) e).getResolver();
            intent.putExtra("EXTRA_MFA_RESOLVER", resolver);
//            setResult(MultiFactorActivity.RESULT_NEEDS_MFA_SIGN_IN, intent);
            dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.emailCreateAccountButton:
            {
                createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
            }break;
            case R.id.emailSignInButton:{
                signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
            }break;
            case R.id.signOutButton:
            {
                signOut();
            }break;
            case R.id.verifyEmailButton:
            {
                sendEmailVerification();
            }break;
            case R.id.reloadButton:{
                reload();
            }break;
            case R.id.check_showpassword:{
                if(checkBoxShowPass.isChecked())
                {
                    mPasswordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edt_newpass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edt_confignewpass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else {
                    mPasswordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edt_newpass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edt_confignewpass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }break;
            case R.id.bt_changePass:{
                showChangepass();
            }break;
            case R.id.bt_canchangepass:{
                hideChangepass();
            }break;
            case R.id.bt_changepassOK:{
                if(validateFormChangePass())
                {
                    updatepass(mAuth.getCurrentUser(),mPasswordField.getText().toString(),edt_newpass.getText().toString());
                }
            }break;
            case R.id.txt_forgotpass:{
                if(validateFormRestPass())
                {
                    resetpass(mEmailField.getText().toString());
                }
            }break;
        }
    }

    private void resetpass(final String email)
    {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "Email reset mật khẩu được gửi thành công đến "+email, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getContext(), "Email reset mật khẩu gửi thất bại", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    private void showProgressBar()
    {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar()
    {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showChangepass()
    {
        view.findViewById(R.id.lin_changepass).setVisibility(View.VISIBLE);
        view.findViewById(R.id.constrain).setVisibility(View.VISIBLE);
        mPasswordField.setVisibility(View.VISIBLE);
        txt_forgotpass.setVisibility(View.GONE);
        checkBoxShowPass.setVisibility(View.VISIBLE);
        mPasswordField.setText(null);
        edt_newpass.setText(null);
        edt_confignewpass.setText(null);
        txt_titleLogin.setText("Đổi mật khẩu");
    }

    private void hideChangepass(){
        mPasswordField.setVisibility(View.GONE);
        view.findViewById(R.id.lin_changepass).setVisibility(View.GONE);
        view.findViewById(R.id.constrain).setVisibility(View.GONE);
        txt_forgotpass.setVisibility(View.VISIBLE);
        checkBoxShowPass.setVisibility(View.GONE);
        mPasswordField.setText(null);
        edt_newpass.setText(null);
        edt_confignewpass.setText(null);
        txt_titleLogin.setText("Đăng nhập");
    }
}
