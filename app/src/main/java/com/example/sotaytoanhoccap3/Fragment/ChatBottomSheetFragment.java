package com.example.sotaytoanhoccap3.Fragment;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.Item.ChatMessage;
import com.example.sotaytoanhoccap3.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class ChatBottomSheetFragment extends BottomSheetDialogFragment {
    View view;
    private FirebaseListAdapter<ChatMessage> adapter;
    private FirebaseAuth mAuth;
    private String ID;
    EditText input;
    FloatingActionButton fab;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottomsheetfragment_chat, container, false);

        ID = MainActivity.selectItemInformation.getStrTitle().replace(" ","").replace(".","") + MainActivity.selectItemInformation.getStrInformation().replace(" ","").replace(".","");
        mAuth = FirebaseAuth.getInstance();
        input = (EditText)view.findViewById(R.id.input);
        fab =
                (FloatingActionButton)view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference(ID)
                        .push()
                        .setValue(new ChatMessage(input.getText().toString(),
                                FirebaseAuth.getInstance()
                                        .getCurrentUser()
                                        .getEmail())
                        );

                // Clear the input
                input.setText("");
            }
        });

        displayChatMessages();

        return view;
    }

    private void displayChatMessages(){
        ListView listOfMessages = (ListView)view.findViewById(R.id.list_of_messages);

        adapter = new FirebaseListAdapter<ChatMessage>(getActivity(), ChatMessage.class,
                R.layout.message, FirebaseDatabase.getInstance().getReference(ID)) {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {
                // Get references to the views of message.xml
                TextView messageText = (TextView)v.findViewById(R.id.message_text);
                TextView messageUser = (TextView)v.findViewById(R.id.message_user);
                TextView messageTime = (TextView)v.findViewById(R.id.message_time);

                // Set their text
                messageText.setText(model.getMessageText());
                messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));
            }
        };

        listOfMessages.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(GetDataFirebase.isNetworkAvailable(getContext())) {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) {
                Toast.makeText(getContext(), "Vui lòng đăng nhập", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        }
        else {
            Toast.makeText(getContext(), "Vui lòng kết nối internet", Toast.LENGTH_SHORT).show();
            dismiss();
        }
        setBackgroundColor();
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            view.findViewById(R.id.linearListinformation).setBackgroundColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            ColorStateList textColorStates = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{
                            Color.parseColor(informationData.getKey(GetStringKey.color)),
                            Color.parseColor(informationData.getKey(GetStringKey.color))
                    });
            fab.setBackgroundTintList(textColorStates);
        }
    }
}
