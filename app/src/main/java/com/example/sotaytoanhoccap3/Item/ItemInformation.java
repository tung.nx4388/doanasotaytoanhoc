package com.example.sotaytoanhoccap3.Item;

import android.os.Parcelable;

import java.util.ArrayList;

public class ItemInformation {
    String strInformation;
    String strTitle;
    Boolean favorite;
    ArrayList<ItemBody> itemBodyArrayList;
    int positionStatus;

    public ArrayList<ItemBody> getItemBodyArrayList() {
        return itemBodyArrayList;
    }

    public void setItemBodyArrayList(ArrayList<ItemBody> itemBodyArrayList) {
        this.itemBodyArrayList = itemBodyArrayList;
    }

    public int getPositionStatus() {
        return positionStatus;
    }

    public void setPositionStatus(int positionStatus) {
        this.positionStatus = positionStatus;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrInformation() {
        return strInformation;
    }

    public void setStrInformation(String strInformation) {
        this.strInformation = strInformation;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public ItemInformation(String strInformation, Boolean favorite,ArrayList<ItemBody> itemBodyArrayList,String strTitle) {
        this.strInformation = strInformation;
        this.favorite = favorite;
        this.itemBodyArrayList = itemBodyArrayList;
        this.strTitle = strTitle;
    }

    public void setALL(ItemInformation itemInformation)
    {
        this.strInformation = itemInformation.getStrInformation();
        this.favorite = itemInformation.getFavorite();
        this.itemBodyArrayList = itemInformation.getItemBodyArrayList();
        this.strTitle = itemInformation.getStrTitle();
    }
}
