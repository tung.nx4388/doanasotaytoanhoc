package com.example.sotaytoanhoccap3.Item;

import android.content.Context;

import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InformationExpandableListDataPump {

    public static HashMap<String, List<ItemInformation>> getData(Context context) {
        HashMap<String, List<ItemInformation>> expandableListDetail = new HashMap<String, List<ItemInformation>>();

        InformationData informationData = new InformationData(context);
        ArrayList<String> itemArrayStrTitle = informationData.getINFDATALIST();

        for(int i = 0;i<itemArrayStrTitle.size();i++)
        {
            expandableListDetail.put(itemArrayStrTitle.get(i),informationData.getInformationData(itemArrayStrTitle.get(i)));
        }

        return expandableListDetail;
    }
}
