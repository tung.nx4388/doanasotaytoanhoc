package com.example.sotaytoanhoccap3.Item;

import java.util.ArrayList;

public class ItemBody {
//    ArrayList<Double> arrayListX,arrayListY;
    String strBody;
    int intMathGrap;
    int local;

    //int mathgrap
    /*0, 1, 2 ,3
    0:null
    1:Title
    2:MathView
    3:GrapView*/

    //int local
    /*0, 1, 2
    0: left
    1: center
    2: right
     */

    public ItemBody(){};

//    public ItemBody(ArrayList<Double> arrayListX, ArrayList<Double> arrayListY, double minX, double maxX, double minY, double maxY, int intMathGrap) {
//        this.arrayListX = arrayListX;
//        this.arrayListY = arrayListY;
//        this.minX = minX;
//        this.minY = minY;
//        this.maxX = maxX;
//        this.maxY = maxY;
//        this.intMathGrap = intMathGrap;
//    }

    public ItemBody(String strBody, int intMathGrap, int local) {
        this.strBody = strBody;
        this.intMathGrap = intMathGrap;
        this.local = local;
    }

//    public ArrayList<Double> getArrayListX() {
//        return arrayListX;
//    }
//
//    public void setArrayListX(ArrayList<Double> arrayListX) {
//        this.arrayListX = arrayListX;
//    }
//
//    public ArrayList<Double> getArrayListY() {
//        return arrayListY;
//    }
//
//    public void setArrayListY(ArrayList<Double> arrayListY) {
//        this.arrayListY = arrayListY;
//    }


    public int getLocal() {
        return local;
    }

    public void setLocal(int local) {
        this.local = local;
    }

    public String getStrBody() {
        return strBody;
    }

    public void setStrBody(String strBody) {
        this.strBody = strBody;
    }

    public int getIntMathGrap() {
        return intMathGrap;
    }

    public void setIntMathGrap(int intMathGrap) {
        this.intMathGrap = intMathGrap;
    }

//    public double getMinX() {
//        return minX;
//    }
//
//    public void setMinX(double minX) {
//        this.minX = minX;
//    }
//
//    public double getMinY() {
//        return minY;
//    }
//
//    public void setMinY(double minY) {
//        this.minY = minY;
//    }
//
//    public double getMaxX() {
//        return maxX;
//    }
//
//    public void setMaxX(double maxX) {
//        this.maxX = maxX;
//    }
//
//    public double getMaxY() {
//        return maxY;
//    }
//
//    public void setMaxY(double maxY) {
//        this.maxY = maxY;
//    }
}
