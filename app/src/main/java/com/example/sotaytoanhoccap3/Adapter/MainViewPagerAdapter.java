package com.example.sotaytoanhoccap3.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.sotaytoanhoccap3.Fragment.FavoriteFragment;
import com.example.sotaytoanhoccap3.Fragment.HomeFragment;
import com.example.sotaytoanhoccap3.Fragment.RecentlyFragment;
import com.example.sotaytoanhoccap3.Fragment.SettingFragment;

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 4;
    public MainViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return HomeFragment.newInstance(0, "Home");
            case 1:
                return RecentlyFragment.newInstance(1, "Recently");
            case 2:
                return FavoriteFragment.newInstance(2, "Favorite");
            case 3:
                return SettingFragment.newInstance(3, "Setting");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
