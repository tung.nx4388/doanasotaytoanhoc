package com.example.sotaytoanhoccap3.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.sotaytoanhoccap3.Activity.BodyActivity;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;

public class InformationCustomExpandableListAdapter extends BaseExpandableListAdapter {
    private Activity context;
    private List<String> expandableListTitle;
    private HashMap<String, List<ItemInformation>> expandableListDetail;

    public InformationCustomExpandableListAdapter(Activity context, List<String> expandableListTitle,
                                                  HashMap<String, List<ItemInformation>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, final ViewGroup parent) {
        final ViewHolderInformation viewHolder;
        viewHolder = new ViewHolderInformation();
        final ItemInformation itemInformation = (ItemInformation) getChild(listPosition, expandedListPosition);
        itemInformation.setStrTitle(expandableListTitle.get(listPosition));
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_information, null);
        }
        viewHolder.cardView = convertView.findViewById(R.id.cardView);
        viewHolder.imgFavorite = convertView.findViewById(R.id.img_favorite);
        viewHolder.txtInformation = convertView.findViewById(R.id.txt_information);
        viewHolder.txtTitleInformation = convertView.findViewById(R.id.txt_titleinformation);

        viewHolder.txtTitleInformation.setVisibility(View.INVISIBLE);
        viewHolder.imgFavorite.setVisibility(View.INVISIBLE);

        InformationData informationData = new InformationData(parent.getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            viewHolder.txtInformation.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            viewHolder.txtTitleInformation.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }

        if(itemInformation.getFavorite())
        {
            viewHolder.imgFavorite.setImageResource(R.mipmap.ic_loveon_foreground);
        }
        else {
            viewHolder.imgFavorite.setImageResource(R.mipmap.ic_loveoff_foreground);
        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralData.putItemRecently(parent.getContext(),itemInformation);
                context.startActivity(new Intent(context, BodyActivity.class));
                context.overridePendingTransition(R.anim.slide_out_bottom, R.anim.no_animal);
                if(GetDataFirebase.isNetworkAvailable(parent.getContext()))
                {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    GetDataFirebase.CretateUID(user,db,parent.getContext());
                }
            }
        });

        viewHolder.txtInformation.setText(itemInformation.getStrInformation());

        viewHolder.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!itemInformation.getFavorite())
                {
                    viewHolder.imgFavorite.setImageResource(R.mipmap.ic_loveon_foreground);
                    itemInformation.setFavorite(true);
                }
                else {
                    viewHolder.imgFavorite.setImageResource(R.mipmap.ic_loveoff_foreground);
                    itemInformation.setFavorite(false);
                }
                GeneralData.setItemFavorite(parent.getContext(), expandableListDetail.get(expandableListTitle.get(listPosition)).get(expandedListPosition));
                if(GetDataFirebase.isNetworkAvailable(parent.getContext()))
                {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    GetDataFirebase.CretateUID(user,db,parent.getContext());
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group_information, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        InformationData informationData = new InformationData(parent.getContext());
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            listTitleTextView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}
