package com.example.sotaytoanhoccap3.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.sotaytoanhoccap3.Fragment.ThemeItemFragment;

import java.util.ArrayList;

public class ViewPagerThemeBackgroundAdapter extends FragmentStatePagerAdapter {
    ArrayList<ThemeItemFragment> themeItemFragments;

    public ViewPagerThemeBackgroundAdapter(@NonNull FragmentManager fm, int behavior, ArrayList<ThemeItemFragment> themeItemFragments) {
        super(fm, behavior);
        this.themeItemFragments = themeItemFragments;
    }

    public ViewPagerThemeBackgroundAdapter(@NonNull FragmentManager fm, ArrayList<ThemeItemFragment> themeItemFragments) {
        super(fm);
        this.themeItemFragments = themeItemFragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return themeItemFragments.get(position);
    }

    @Override
    public int getCount() {
        return themeItemFragments.size();
    }
}
