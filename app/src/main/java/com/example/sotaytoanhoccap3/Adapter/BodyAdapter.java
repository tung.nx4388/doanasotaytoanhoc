package com.example.sotaytoanhoccap3.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Item.ItemBody;
import com.example.sotaytoanhoccap3.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

import io.github.kexanie.library.MathView;

public class BodyAdapter extends BaseAdapter {
    ArrayList<ItemBody> itemBodyArrayList;
    Activity activity;

    public BodyAdapter(ArrayList<ItemBody> itemBodyArrayList,Activity activity) {
        this.activity = activity;
        this.itemBodyArrayList = itemBodyArrayList;
    }

    @Override
    public int getCount() {
        return itemBodyArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemBodyArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        switch (itemBodyArrayList.get(position).getIntMathGrap())
        {
            case 1:
            {
                convertView = View.inflate(parent.getContext(),R.layout.mathview_item_bodytitle,null);

                viewHolder.textView = convertView.findViewById(R.id.txt_titlebody);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    switch (itemBodyArrayList.get(position).getLocal()) {
                        case 1: {
                            viewHolder.textView.setForegroundGravity(Gravity.START);
                        }
                        break;
                        case 2: {
                            viewHolder.textView.setForegroundGravity(Gravity.CENTER);
                            viewHolder.textView.setPadding(0, 0, 0, 0);
                        }
                        break;
                        case 3: {
                            viewHolder.textView.setForegroundGravity(Gravity.END);
                        }
                        break;
                    }
                }
                viewHolder.textView.setText(itemBodyArrayList.get(position).getStrBody());
            }
            break;
            case 2:
            {
                convertView = View.inflate(parent.getContext(), R.layout.mathview_item_body,null);

                viewHolder.mathView = convertView.findViewById(R.id.mathviewbody);
                switch (itemBodyArrayList.get(position).getLocal())
                {
                    case 2:{
                        viewHolder.mathView.config(
                                "MathJax.Hub.Config({\n"+
                                        "  CommonHTML: { linebreaks: { automatic: true } },\n"+
                                        "  \"HTML-CSS\": { linebreaks: { automatic: true } },\n"+
                                        "         SVG: { linebreaks: { automatic: true } },\n"+
                                        "            displayAlign: \"center\"\n" +
                                        "});");
                    }break;
                    case 3:{
                        viewHolder.mathView.config(
                                "MathJax.Hub.Config({\n"+
                                        "  CommonHTML: { linebreaks: { automatic: true } },\n"+
                                        "  \"HTML-CSS\": { linebreaks: { automatic: true } },\n"+
                                        "         SVG: { linebreaks: { automatic: true } },\n"+
                                        "            displayAlign: \"right\"\n" +
                                        "});");
                    }break;
                    default:{
                        viewHolder.mathView.config(
                                "MathJax.Hub.Config({\n"+
                                        "  CommonHTML: { linebreaks: { automatic: true } },\n"+
                                        "  \"HTML-CSS\": { linebreaks: { automatic: true } },\n"+
                                        "         SVG: { linebreaks: { automatic: true } },\n"+
                                        "            displayAlign: \"left\"\n" +
                                        "});");
                    }
                }

                viewHolder.mathView.setText(itemBodyArrayList.get(position).getStrBody());
            }break;
            case 3:
            {
                InformationData informationData = new InformationData(parent.getContext());
                convertView = View.inflate(parent.getContext(),R.layout.imageviewbody,null);
                viewHolder.imageView = convertView.findViewById(R.id.imageviewbody);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    switch (itemBodyArrayList.get(position).getLocal()) {
                        case 1: {
                            viewHolder.imageView.setForegroundGravity(Gravity.START);
                        }
                        break;
                        case 2: {
                            viewHolder.imageView.setForegroundGravity(Gravity.CENTER);
                            viewHolder.imageView.setPadding(0, 0, 0, 0);
                        }
                        break;
                        case 3: {
                            viewHolder.imageView.setForegroundGravity(Gravity.END);
                        }
                        break;
                    }
                }
                viewHolder.imageView.setImageBitmap(Bitmap.createScaledBitmap(informationData.loadImageFromStorage(itemBodyArrayList.get(position).getStrBody()),600,600,false));
//                convertView = View.inflate(parent.getContext(),R.layout.grapviewbody,null);
//
//                viewHolder.graphView = convertView.findViewById(R.id.grapviewbody);
//                LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
//                if(itemBodyArrayList.get(position).getArrayListX().size() == itemBodyArrayList.get(position).getArrayListY().size())
//                {
//                    for (int i = 0; i < itemBodyArrayList.get(position).getArrayListY().size();i++)
//                    {
//                        series.appendData(new DataPoint(itemBodyArrayList.get(position).getArrayListX().get(i),itemBodyArrayList.get(position).getArrayListY().get(i)),
//                                true,itemBodyArrayList.get(position).getArrayListY().size(),true);
//                    }
//                }
//                viewHolder.graphView.getViewport().setXAxisBoundsManual(true);
//                viewHolder.graphView.getViewport().setMinX(itemBodyArrayList.get(position).getMinX());
//                viewHolder.graphView.getViewport().setMaxX(itemBodyArrayList.get(position).getMaxX());
//
//                viewHolder.graphView.getViewport().setYAxisBoundsManual(true);
//                viewHolder.graphView.getViewport().setMinY(itemBodyArrayList.get(position).getMinY());
//                viewHolder.graphView.getViewport().setMaxY(itemBodyArrayList.get(position).getMaxY());

//                viewHolder.graphView.getViewport().setScalable(true);
//                viewHolder.graphView.getViewport().setScalableY(true);
//                viewHolder.graphView.addSeries(series);
            }break;
        }
        return convertView;
    }

    class ViewHolder{
        MathView mathView;
        GraphView graphView;
        TextView textView;
        ImageView imageView;
        WebView webView;
    }
}
