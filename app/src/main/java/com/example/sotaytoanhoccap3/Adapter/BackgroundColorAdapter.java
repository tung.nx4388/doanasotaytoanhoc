package com.example.sotaytoanhoccap3.Adapter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.cardview.widget.CardView;

import com.example.sotaytoanhoccap3.Fragment.SetColorBackgroundDialogFragment;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticCardView;

public class BackgroundColorAdapter extends BaseAdapter {
    String[] listColor;
    ElasticCardView cardViewButtonLoca;

    public BackgroundColorAdapter( String[] listColor) {
        this.listColor = listColor;
    }

    @Override
    public int getCount() {
        return listColor.length;
    }

    @Override
    public Object getItem(int position) {
        return listColor[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = View.inflate(parent.getContext(), R.layout.item_colorbackground,null);
        }

        final ElasticCardView cardviewButton = convertView.findViewById(R.id.cardViewButton_colorbackground);
        CardView cardViewColor = convertView.findViewById(R.id.cardViewColor);
        View viewTop = convertView.findViewById(R.id.viewTopColor);
        View viewBottom = convertView.findViewById(R.id.viewBottomColor);
        View viewStart = convertView.findViewById(R.id.viewStartColor);
        View viewEnd = convertView.findViewById(R.id.viewEndColor);

        cardviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardViewButtonLoca!=null)
                {
                    cardViewButtonLoca.setCardBackgroundColor(Color.WHITE);
                }
                cardviewButton.setCardBackgroundColor(Color.GRAY);
                cardViewButtonLoca = cardviewButton;
                SetColorBackgroundDialogFragment.framePreview.setVisibility(View.VISIBLE);
                SetColorBackgroundDialogFragment.setBackgroundPreview(listColor[position]);
            }
        });

        cardViewColor.setCardBackgroundColor(Color.parseColor((String) getItem(position)));
        viewTop.setBackgroundColor(Color.parseColor((String) getItem(position)));
        viewBottom.setBackgroundColor(Color.parseColor((String) getItem(position)));
        viewStart.setBackgroundColor(Color.parseColor((String) getItem(position)));
        viewEnd.setBackgroundColor(Color.parseColor((String) getItem(position)));

        return convertView;
    }
}
