package com.example.sotaytoanhoccap3.Adapter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.sotaytoanhoccap3.Fragment.SetThemeBackgroundDialogFragment;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticCardView;

import java.util.ArrayList;

public class BackgroundThemenormalAdapter extends BaseAdapter {
    ArrayList<Integer> arrayListImg;
    ElasticCardView elasticCardViewChoice;

    public BackgroundThemenormalAdapter(ArrayList<Integer> arrayListImg) {
        this.arrayListImg = arrayListImg;
    }

    @Override
    public int getCount() {
        return arrayListImg.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListImg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = View.inflate(parent.getContext(), R.layout.item_themebackground,null);
        }
        ImageView imageView = convertView.findViewById(R.id.img_theme);
        final ElasticCardView elasticCardView = convertView.findViewById(R.id.cardViewButton_themebackground);

        imageView.setImageResource(arrayListImg.get(position));
        elasticCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(elasticCardViewChoice!=null)
                {
                    elasticCardViewChoice.setCardBackgroundColor(Color.WHITE);
                }
                SetThemeBackgroundDialogFragment.resImagechoice = arrayListImg.get(position);
                elasticCardView.setCardBackgroundColor(Color.GRAY);
                elasticCardViewChoice = elasticCardView;
            }
        });

        return convertView;
    }
}
