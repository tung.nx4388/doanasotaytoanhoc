package com.example.sotaytoanhoccap3.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class ListInformationAdapter extends BaseAdapter {
    public static int pos;
    ArrayList<ItemInformation> informations;
    Activity activity;

    public ListInformationAdapter(ArrayList<ItemInformation> informations,Activity activity) {
        this.informations = informations;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return informations.size();
    }

    @Override
    public Object getItem(int position) {
        return informations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        informations.get(position).setStrTitle(MainActivity.selectItemInformation.getStrTitle());
        final ViewHolderInformation viewHolderInformation;
        viewHolderInformation = new ViewHolderInformation();
        if(convertView == null)
        {
            convertView = View.inflate(parent.getContext(),R.layout.list_item_information,null);
        }
        viewHolderInformation.cardView = convertView.findViewById(R.id.cardView);
        viewHolderInformation.imgFavorite = convertView.findViewById(R.id.img_favorite);
        viewHolderInformation.txtInformation = convertView.findViewById(R.id.txt_information);
        viewHolderInformation.txtTitleInformation = convertView.findViewById(R.id.txt_titleinformation);

        viewHolderInformation.txtTitleInformation.setVisibility(View.INVISIBLE);

        if(informations.get(position).getFavorite())
        {
            viewHolderInformation.imgFavorite.setImageResource(R.mipmap.ic_loveon_foreground);
        }
        else {
            viewHolderInformation.imgFavorite.setImageResource(R.mipmap.ic_loveoff_foreground);
        }

        viewHolderInformation.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(informations.get(position).getStrInformation().compareTo("Chưa có dữ liệu")!=0)
                {
                    GeneralData.putItemRecently(parent.getContext(),informations.get(position));
                    MainActivity.selectItemInformation = informations.get(position);
                    activity.setResult(Activity.RESULT_OK);
                    activity.finish();
                    activity.overridePendingTransition(R.anim.no_animal, R.anim.slide_in_right);
                    if(GetDataFirebase.isNetworkAvailable(parent.getContext()))
                    {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        GetDataFirebase.CretateUID(user,db,parent.getContext());
                    }
                }
            }
        });

        viewHolderInformation.txtInformation.setText(informations.get(position).getStrInformation());
        if(informations.get(position).getStrInformation().compareTo("Chưa có dữ liệu")==0)
        {
            viewHolderInformation.imgFavorite.setVisibility(View.INVISIBLE);
        }


        viewHolderInformation.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!informations.get(position).getFavorite())
                {
                    viewHolderInformation.imgFavorite.setImageResource(R.mipmap.ic_loveon_foreground);
                    informations.get(position).setFavorite(true);
                }
                else {
                    viewHolderInformation.imgFavorite.setImageResource(R.mipmap.ic_loveoff_foreground);
                    informations.get(position).setFavorite(false);
                }
                GeneralData.setItemFavorite(parent.getContext(),informations.get(position));
                if(GetDataFirebase.isNetworkAvailable(parent.getContext()))
                {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    GetDataFirebase.CretateUID(user,db,parent.getContext());
                }
            }
        });

        if(informations.get(position).getStrInformation().compareTo(MainActivity.selectItemInformation.getStrInformation()) == 0)
        {
            pos = position;
            viewHolderInformation.cardView.setCardBackgroundColor(Color.parseColor("#37000000"));
        }

        return convertView;
    }
}
