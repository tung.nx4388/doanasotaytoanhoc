package com.example.sotaytoanhoccap3.Adapter;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticCardView;

import java.util.ArrayList;

public class BackgroundThemeStorageAdapter extends BaseAdapter {
    ElasticCardView elasticCardViewChoice;
    public static String abc;

    /** The context. */
    private Activity context;
    private ArrayList<String> images;
    public BackgroundThemeStorageAdapter(Activity localContext) {
        context = localContext;
        images = getAllShownImagesPath(context);
    }

    public int getCount() {
        return images.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = View.inflate(parent.getContext(), R.layout.item_themebackground,null);
        }
        ImageView imageView = convertView.findViewById(R.id.img_theme);
        final ElasticCardView elasticCardView = convertView.findViewById(R.id.cardViewButton_themebackground);
        final InformationData informationData = new InformationData(context);
        elasticCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(elasticCardViewChoice!=null)
                {
                    elasticCardViewChoice.setCardBackgroundColor(Color.WHITE);
                }
//                SetThemeBackgroundDialogFragment.resImagechoice = arrayListImg.get(position);
                elasticCardView.setCardBackgroundColor(Color.GRAY);
                elasticCardViewChoice = elasticCardView;
                abc = images.get(position);
            }
        });

        Glide.with(context).load(images.get(position))
                .centerCrop()
                .into(imageView);

        return convertView;
    }

    /**
     * Getting All Images Path.
     *
     * @param activity
     *            the activity
     * @return ArrayList with images Path
     */
    private ArrayList<String> getAllShownImagesPath(Activity activity) {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        try {
            String absolutePathOfImage = null;
            uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

            String[] projection = { MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

            cursor = activity.getContentResolver().query(uri, projection, null,
                    null, null);

            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            column_index_folder_name = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data);

                listOfAllImages.add(absolutePathOfImage);
            }
        }catch (Exception ex)
        {
            Toast.makeText(activity, "Bạn chưa mở quyền xem hình ảnh", Toast.LENGTH_SHORT).show();
        }

        return listOfAllImages;
    }
}
