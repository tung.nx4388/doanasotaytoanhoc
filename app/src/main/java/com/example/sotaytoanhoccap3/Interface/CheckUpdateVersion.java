package com.example.sotaytoanhoccap3.Interface;

public interface CheckUpdateVersion {
    void setUpdate(Boolean check);
    void setStVersion(String stVersion);
    void setDowndloadDataFull(Boolean check);
}
