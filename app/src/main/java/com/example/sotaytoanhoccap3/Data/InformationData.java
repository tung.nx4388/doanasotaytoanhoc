package com.example.sotaytoanhoccap3.Data;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class InformationData {
    private Context context;
    private SharedPreferences app_prefs;
    private String INFORMATIONDATA = "informationdata";
//    private String INFDATADAISO = "infdatadaiso", INFDATALG = "infdatalg",INFDATAGT="infdatagt";
    private String INFDATARECENTLY = "infdatarecently";
    private String INFDATALIST ="infdatalist";
    private String INFFAVORITELOCAL = "inffavoritelocal";
    private Gson gson;
    private Type arrayListTypeTokenItemInfor;
    private Type arrayListTypeTokenString;

    public InformationData(Context context) {
        app_prefs = context.getSharedPreferences(INFORMATIONDATA,
                Context.MODE_PRIVATE);
        gson = new Gson();
        arrayListTypeTokenItemInfor = new TypeToken<ArrayList<ItemInformation>>(){}.getType();
        arrayListTypeTokenString = new TypeToken<ArrayList<String>>(){}.getType();
        this.context = context;
    }

    //ListString
    public ArrayList<String> getListString(String keyString)
    {
        return gson.fromJson(app_prefs.getString(keyString, ""),  arrayListTypeTokenString);
    }

    public void putListString(ArrayList<String> listString,String keyString)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(keyString,gson.toJson(listString));
        edit.commit();
    }

    public String getKey(String key)
    {
        return app_prefs.getString(key, "");
    }

    public void putKey(String key, String data)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(key,data);
        edit.commit();
    }

    //png bitmap
    public String saveToInternalStorage(Bitmap bitmapImage,String image){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,image);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            Log.d("abcabc","ERROR");
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Log.d("abcabc","ERROR");
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String image)
    {
        String path = getBitmap(image);
        try {
            File f=new File(path, image);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            Log.d("abcabc","ERROR2");
            Log.d("abc",e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public String getBitmap(String image)
    {
        return app_prefs.getString(image, "");
    }

    public void putBitmap(Bitmap bitmapImage,String image)
    {
        String path = saveToInternalStorage(bitmapImage, image);
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(image,path);
        edit.commit();
    }

    //item
    public void putInformationData(ArrayList<ItemInformation> itemInformations,String strTile)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(strTile,gson.toJson(itemInformations));
        edit.commit();
    }

    public ArrayList<ItemInformation> getInformationData(String strTile)
    {
        return gson.fromJson(app_prefs.getString(strTile, ""),  arrayListTypeTokenItemInfor);
    }

    //title
    public ArrayList<String> getINFDATALIST()
    {
        return gson.fromJson(app_prefs.getString(INFDATALIST, ""),  arrayListTypeTokenString);
    }

    public void putINFDATALIST(ArrayList<String> itemInformations)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(INFDATALIST,gson.toJson(itemInformations));
        edit.commit();
    }

    //recently
    public void putInformationDataRecently (ArrayList<ItemInformation> itemInformations)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(INFDATARECENTLY,gson.toJson(itemInformations));
        edit.commit();
    }

    public ArrayList<ItemInformation> getInformationDataRecently()
    {
        return gson.fromJson(app_prefs.getString(INFDATARECENTLY, ""),  arrayListTypeTokenItemInfor);
    }

    //favoritelocal
    public void putInformationDataFavorite (ArrayList<ItemInformation> itemInformations)
    {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(INFFAVORITELOCAL,gson.toJson(itemInformations));
        edit.commit();
    }

    public ArrayList<ItemInformation> getInformationDataFavorite()
    {
        return gson.fromJson(app_prefs.getString(INFFAVORITELOCAL, ""),  arrayListTypeTokenItemInfor);
    }

//    public void putInformationDataDaiso(ArrayList<ItemInformation> itemInformations)
//    {
//        SharedPreferences.Editor edit = app_prefs.edit();
//        edit.putString(INFDATADAISO,new Gson().toJson(itemInformations));
//        edit.commit();
//    }
//
//    public ArrayList<ItemInformation> getInformationDataDaiso()
//    {
//        return new Gson().fromJson(app_prefs.getString(INFDATADAISO, ""),  new TypeToken<ArrayList<ItemInformation>>(){}.getType());
//    }
//
//    public void putInformationDataLuonggiac(ArrayList<ItemInformation> itemInformations)
//    {
//        SharedPreferences.Editor edit = app_prefs.edit();
//        edit.putString(INFDATALG,new Gson().toJson(itemInformations));
//        edit.commit();
//    }
//
//    public ArrayList<ItemInformation> getInformationDataLuonggiac()
//    {
//        return new Gson().fromJson(app_prefs.getString(INFDATALG, ""),  new TypeToken<ArrayList<ItemInformation>>(){}.getType());
//    }
//
//    public void putInformationDataGiaitich(ArrayList<ItemInformation> itemInformations)
//    {
//        SharedPreferences.Editor edit = app_prefs.edit();
//        edit.putString(INFDATAGT,new Gson().toJson(itemInformations));
//        edit.commit();
//    }
//
//    public ArrayList<ItemInformation> getInformationDataGiaitich()
//    {
//        return new Gson().fromJson(app_prefs.getString(INFDATAGT, ""),  new TypeToken<ArrayList<ItemInformation>>(){}.getType());
//    }
}
