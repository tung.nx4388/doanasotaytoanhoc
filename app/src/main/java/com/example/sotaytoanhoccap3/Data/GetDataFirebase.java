package com.example.sotaytoanhoccap3.Data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Interface.CheckUpdateVersion;
import com.example.sotaytoanhoccap3.Item.ItemBody;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetDataFirebase {
    static String LOG = "abcabc",ERROR = "ERROR";
    static InformationData informationData;
    static FirebaseFirestore db;
    static Handler handler;
    static Activity activity;
    static long Milis = 5000;
    static TextView txtLoad;
    static ProgressBar progressBar;
    static StorageReference storageRef;
    static ArrayList<String> version;
    static String[] arraylistUpdateLoca;
    static String type;

    public static void GetFullData(FirebaseFirestore db, StorageReference storageRef, Activity activity, TextView txtLoad, ProgressBar progressBar) {
        txtLoad.setText("Xin chờ để load dữ liệu 0/3");

        informationData = new InformationData(activity);
        if(informationData.getINFDATALIST() == null) {
            informationData.putINFDATALIST(new ArrayList<String>());
        }
        if(informationData.getInformationDataRecently() == null)
        {
            informationData.putInformationDataRecently(new ArrayList<ItemInformation>());
        }
        GetDataFirebase.db = db;
        GetDataFirebase.storageRef = storageRef;
        GetDataFirebase.activity = activity;
        GetDataFirebase.txtLoad = txtLoad;
        GetDataFirebase.progressBar = progressBar;

        type = "full";

        handler = new Handler();
        handler.postDelayed(runnableDownloadTitle,Milis);
    }

    public static void GetVersion(FirebaseFirestore db, final Context context)
    {
            handler = new Handler();
            informationData = new InformationData(context);
            final CheckUpdateVersion checkUpdateVersion = (CheckUpdateVersion) context;
            db.collection(GetStringKey.Data).document(GetStringKey.Listitle)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot documentSnapshot = task.getResult();
                                if (documentSnapshot.exists()) {
                                    version = (ArrayList<String>) documentSnapshot.get(GetStringKey.version);
                                    ArrayList<String> listupdate = (ArrayList<String>) documentSnapshot.get(GetStringKey.ListUpdate);
                                    informationData.putListString(version, GetStringKey.listversion);
                                    informationData.putListString(listupdate, GetStringKey.ListUpdate);
                                    checkUpdateVersion.setStVersion(version.get(version.size() - 1));
                                }
                            } else {
                                Log.d(ERROR, "Check Version");
                            }
                        }
                    });
    }

    public static void UpdateVersion(FirebaseFirestore db, StorageReference storageRef, Activity activity, TextView txtLoad, ProgressBar progressBar)
    {
        informationData = new InformationData(activity);
        GetDataFirebase.db = db;
        GetDataFirebase.storageRef = storageRef;
        GetDataFirebase.activity = activity;
        GetDataFirebase.txtLoad = txtLoad;
        GetDataFirebase.progressBar = progressBar;

        type = "full";

        if(informationData.getListString(GetStringKey.listversion).size()>1)
        {
            if(Double.parseDouble(informationData.getListString(GetStringKey.listversion).get(informationData.getListString(GetStringKey.listversion).size()-2))
                    > Double.parseDouble(informationData.getKey(GetStringKey.version)))
            {
                GetFullData(db,storageRef,activity,txtLoad,progressBar);
            }
            else {
                switch (informationData.getListString(GetStringKey.ListUpdate).get(informationData.getListString(GetStringKey.ListUpdate).size()-1))
                {
                    case "all":
                    {
                        GetFullData(db,storageRef,activity,txtLoad,progressBar);
                    }break;
                    case "addnew":
                    {
                        arraylistUpdateLoca = new String[2];
                        arraylistUpdateLoca[0] = "null";
                        handler.postDelayed(runnableUpdaetTitle,Milis);
                    }break;
                    default:{
                        arraylistUpdateLoca = informationData.getListString(GetStringKey.ListUpdate).get(informationData.getListString(GetStringKey.ListUpdate).size()-1).split(" ");
                        handler.postDelayed(runnableUpdaetTitle,Milis);
                    }
                }
            }
        }
        else {
            GetFullData(db,storageRef,activity,txtLoad,progressBar);
        }
    }

    public static void CretateUID(FirebaseUser user,FirebaseFirestore db,Context context)
    {
        informationData = new InformationData(context);
        Gson gson = new Gson();
        if(user!=null)
        {
            ArrayList<ItemInformation> itemListRecently = informationData.getInformationDataRecently();
            ArrayList<ItemInformation> itemListFavorite = GeneralData.getListItemFavorite(context);
                for (int i = 0;i<itemListFavorite.size();i++)
                {
                    itemListFavorite.get(i).setItemBodyArrayList(new ArrayList<ItemBody>());
                }
            if(itemListRecently!=null)
            {
                for (int i = 0;i<itemListRecently.size();i++)
                {
                    itemListRecently.get(i).setItemBodyArrayList(new ArrayList<ItemBody>());
                }
            }

            Map<String, Object> data = new HashMap<>();
            data.put(GetStringKey.listrecently, gson.toJson(itemListRecently));
            data.put(GetStringKey.listfavorite,gson.toJson(itemListFavorite));
            data.put(GetStringKey.version,informationData.getKey(GetStringKey.version));

            db.collection("User").document(user.getUid())
                    .set(data, SetOptions.merge());
        }
    }

    public static void UpdateRecentlyFavoriteSignin(FirebaseUser user, FirebaseFirestore db, final Context context)
    {
        informationData = new InformationData(context);
        if(user!=null)
        {
            db.collection(GetStringKey.User).document(user.getUid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document1 = task.getResult();
                                if(document1.exists())
                                {
                                    String listrecently = (String) document1.get(GetStringKey.listrecently);
                                    String listfavorite = (String) document1.get(GetStringKey.listfavorite);
                                    String version = (String) document1.get(GetStringKey.version);
                                    informationData.putKey(GetStringKey.version,version);
                                    GeneralData.setFavoriteFirebaseToLocal(context,listfavorite);
                                    GeneralData.setRecentlyFirebaseToLocal(context,listrecently);
                                }
                            } else {
                                Log.d(LOG, "ErrorUpdateRecentlyFavorite");
                            }
                        }
                    });
        }
    }

    static Runnable runnableUpdaetTitle = new Runnable() {
        @Override
        public void run() {
            txtLoad.setText("Xin chờ để load dữ liệu 1/3");
            if(arraylistUpdateLoca[0].compareTo("null")==0)
            {
                int loca = informationData.getINFDATALIST().size()-1;
                arraylistUpdateLoca[1] = ""+loca;
            }
            db.collection(GetStringKey.Data).document(GetStringKey.Listitle)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document1 = task.getResult();
                                ArrayList<String> arrayListTitile = (ArrayList<String>) document1.get(GetStringKey.Listitle);
                                informationData.putINFDATALIST(arrayListTitile);
                                progressBar.setProgress(33);
//                                    Log.d(LOG, arrayListTitile.get(0));
                            } else {
                                Log.d(LOG, "Error1");
                            }
                        }
                    });
            handler.postDelayed(runnableUppdateTitleBody,Milis);
        }
    };

    static  Runnable runnableUppdateTitleBody = new Runnable() {
        @Override
        public void run() {
            int zzz = 0;
            int size = 0;
            txtLoad.setText("Xin chờ để load dữ liệu 2/3");
            final ArrayList<String> arrayListTitile = informationData.getINFDATALIST();
            if (arraylistUpdateLoca[0].compareTo("null") == 0) {
                zzz = Integer.parseInt(arraylistUpdateLoca[1]);
                size = arrayListTitile.size();
                final int processss = 33 / size;
                for (; zzz < size; zzz++) {
                    final int finalZ = zzz;
                    Log.d(LOG, arrayListTitile.get(finalZ));
                    db.collection(arrayListTitile.get(finalZ)).document(GetStringKey.Data)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document2 = task.getResult();
                                        if (document2.exists()) {
                                            ArrayList<String> arrayListTitleHeadBody = (ArrayList<String>) document2.get(GetStringKey.arraylistbody);

                                            Log.d(LOG, "" + arrayListTitleHeadBody.size());
                                            ArrayList<ItemInformation> itemInformationArrayList = new ArrayList<>();
                                            if (type.compareTo("full") == 0) {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            new ArrayList<ItemBody>(), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            } else {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            GeneralData.getItemBody(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            }
                                            informationData.putInformationData(itemInformationArrayList, arrayListTitile.get(finalZ));
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        } else {
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                            informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                            Log.d(LOG, "Error2");
                                        }
                                    } else {
                                        progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                        Log.d(LOG, "Error2");
                                    }
                                }
                            });
                }
            } else {
                zzz = 0;
                size = arraylistUpdateLoca.length;
                final int processss = 33 / size;
                for (; zzz < size; zzz++) {
                    final int finalZ = Integer.parseInt(arraylistUpdateLoca[zzz]);
                    Log.d(LOG, arrayListTitile.get(finalZ));
                    db.collection(arrayListTitile.get(finalZ)).document(GetStringKey.Data)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document2 = task.getResult();
                                        if (document2.exists()) {
                                            ArrayList<String> arrayListTitleHeadBody = (ArrayList<String>) document2.get(GetStringKey.arraylistbody);
                                            ArrayList<ItemInformation> itemInformationArrayList = new ArrayList<>();
                                            Log.d(LOG, "" + arrayListTitleHeadBody.size());
                                            if (type.compareTo("full") == 0) {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            new ArrayList<ItemBody>(), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            } else {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            GeneralData.getItemBody(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            }
                                            informationData.putInformationData(itemInformationArrayList, arrayListTitile.get(finalZ));
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        } else {
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                            informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                            Log.d(LOG, "Error2");
                                        }
                                    } else {
                                        progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                        Log.d(LOG, "Error2");
                                    }
                                }
                            });
                }
            }
            if (type.compareTo("full") == 0) {
                handler.postDelayed(runnableUpdateBody, Milis);
            }
        }
    };

    static Runnable runnableUpdateBody = new Runnable() {
        @Override
        public void run() {
            int zzz = 0;
            int size = 0;
            txtLoad.setText("Xin chờ để load dữ liệu 3/3");
            final ArrayList<String> arrayListTitile = informationData.getINFDATALIST();
            if(arraylistUpdateLoca[0].compareTo("null")==0)
            {
                zzz = Integer.parseInt(arraylistUpdateLoca[1]);
                size = arrayListTitile.size();
                int processsssi = 33/size;
                for (int i = zzz; i < size; i++) {
                    final ArrayList<ItemInformation> informationArrayList = informationData.getInformationData(arrayListTitile.get(i));
                    int processsssk = processsssi;
                    if(informationArrayList.size()!=0)
                    {
                        processsssk = processsssi/informationArrayList.size();
                    }
                    for (int k = 0; k < informationArrayList.size(); k++) {
                        final int finalI = i;
                        final int finalK = k;
                        final int finalProcesssssk = processsssk;
                        db.collection(arrayListTitile.get(i)).document(informationArrayList.get(k).getStrInformation())
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document3 = task.getResult();
                                            if(document3.exists()) {
                                                ArrayList<String> arrayListMathView = (ArrayList<String>) document3.get(GetStringKey.arraylistmathview);
                                                ArrayList<String> arrayListTitleBody = (ArrayList<String>) document3.get(GetStringKey.arraystrtitle);
                                                ArrayList<String> arrayimagelist = (ArrayList<String>) document3.get(GetStringKey.arrayimagelist);
                                                String strstt = (String) document3.get(GetStringKey.strstt);
                                                String strlocal = (String) document3.get(GetStringKey.strlocal);
                                                String[] arrayListStt = strstt.split(" ");
                                                String[] arrayListLocal = strlocal.split(" ");
                                                ArrayList<ItemBody> itemBodyArrayList = new ArrayList<>();
                                                int loca1 = 0, loca2 = 0, loca3 = 0;
                                                for (int i = 0; i < arrayListStt.length; i++) {
                                                    //1:title 2:mathview 3:grapview
                                                    switch (arrayListStt[i]) {
                                                        case "1": {
                                                            itemBodyArrayList.add(new ItemBody(arrayListTitleBody.get(loca1), 1,Integer.parseInt(arrayListLocal[i])));
                                                            loca1++;
                                                        }
                                                        break;
                                                        case "2": {
                                                            itemBodyArrayList.add(new ItemBody(arrayListMathView.get(loca2), 2,Integer.parseInt(arrayListLocal[i])));
                                                            loca2++;
                                                        }
                                                        break;
                                                        case "3": {
                                                            final String image = arrayimagelist.get(loca3).replace("/","");
                                                            StorageReference islandRef = storageRef.child(arrayimagelist.get(loca3));
                                                            final long ONE_MEGABYTE = 1024 * 1024;
                                                            final int finalLoca = loca3;
                                                            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                                                @Override
                                                                public void onSuccess(byte[] bytes) {
                                                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                                                    informationData.putBitmap(bitmap,image);
                                                                }
                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception exception) {
                                                                    // Handle any errors
                                                                }
                                                            });
                                                            itemBodyArrayList.add(new ItemBody(image,3,Integer.parseInt(arrayListLocal[i])));
                                                            loca3++;
                                                        }
                                                        break;
                                                    }
                                                }
                                                informationArrayList.get(finalK).setItemBodyArrayList(itemBodyArrayList);
                                                informationData.putInformationData(informationArrayList, arrayListTitile.get(finalI));
                                                progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                            }
                                            else
                                            {
                                                progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                                Log.d(LOG, "Error3");
                                            }
                                        } else {
                                            progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                            Log.d(LOG, "Error3");
                                        }
                                    }
                                });
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else {
                zzz = 0;
                size = arraylistUpdateLoca.length;
                int processsssi = 33/size;
                for (int o = zzz; o < size; o++) {
                    int i = Integer.parseInt(arraylistUpdateLoca[o]);
                    final ArrayList<ItemInformation> informationArrayList = informationData.getInformationData(arrayListTitile.get(i));
                    int processsssk = processsssi;
                    if(informationArrayList.size()!=0)
                    {
                        processsssk = processsssi/informationArrayList.size();
                    }
                    for (int k = 0; k < informationArrayList.size(); k++) {
                        final int finalI = i;
                        final int finalK = k;
                        final int finalProcesssssk = processsssk;
                        db.collection(arrayListTitile.get(i)).document(informationArrayList.get(k).getStrInformation())
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document3 = task.getResult();
                                            if(document3.exists()) {
                                                ArrayList<String> arrayListMathView = (ArrayList<String>) document3.get(GetStringKey.arraylistmathview);
                                                ArrayList<String> arrayListTitleBody = (ArrayList<String>) document3.get(GetStringKey.arraystrtitle);
                                                ArrayList<String> arrayimagelist = (ArrayList<String>) document3.get(GetStringKey.arrayimagelist);
                                                String strstt = (String) document3.get(GetStringKey.strstt);
                                                String strlocal = (String) document3.get(GetStringKey.strlocal);
                                                String[] arrayListStt = strstt.split(" ");
                                                String[] arrayListLocal = strlocal.split(" ");
                                                ArrayList<ItemBody> itemBodyArrayList = new ArrayList<>();
                                                int loca1 = 0, loca2 = 0, loca3 = 0;
                                                for (int i = 0; i < arrayListStt.length; i++) {
                                                    //1:title 2:mathview 3:grapview
                                                    switch (arrayListStt[i]) {
                                                        case "1": {
                                                            itemBodyArrayList.add(new ItemBody(arrayListTitleBody.get(loca1), 1,Integer.parseInt(arrayListLocal[i])));
                                                            loca1++;
                                                        }
                                                        break;
                                                        case "2": {
                                                            itemBodyArrayList.add(new ItemBody(arrayListMathView.get(loca2), 2,Integer.parseInt(arrayListLocal[i])));
                                                            loca2++;
                                                        }
                                                        break;
                                                        case "3": {
                                                            final String image = arrayimagelist.get(loca3).replace("/","");
                                                            StorageReference islandRef = storageRef.child(arrayimagelist.get(loca3));
                                                            final long ONE_MEGABYTE = 1024 * 1024;
                                                            final int finalLoca = loca3;
                                                            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                                                @Override
                                                                public void onSuccess(byte[] bytes) {
                                                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                                                    informationData.putBitmap(bitmap,image);
                                                                }
                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception exception) {
                                                                    // Handle any errors
                                                                }
                                                            });
                                                            itemBodyArrayList.add(new ItemBody(image,3,Integer.parseInt(arrayListLocal[i])));
                                                            loca3++;
                                                        }
                                                        break;
                                                    }
                                                }
                                                informationArrayList.get(finalK).setItemBodyArrayList(itemBodyArrayList);
                                                informationData.putInformationData(informationArrayList, arrayListTitile.get(finalI));
                                                progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                            }
                                            else
                                            {
                                                progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                                Log.d(LOG, "Error3");
                                            }
                                        } else {
                                            progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                            Log.d(LOG, "Error3");
                                        }
                                    }
                                });
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            handler.postDelayed(runnableStartHome,Milis);
        }
    };

    static Runnable runnableDownloadTitle = new Runnable() {
        @Override
        public void run() {
            txtLoad.setText("Xin chờ để load dữ liệu 1/3");
            db.collection(GetStringKey.Data).document(GetStringKey.Listitle)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document1 = task.getResult();
                                ArrayList<String> arrayListTitile = (ArrayList<String>) document1.get(GetStringKey.Listitle);
                                ArrayList<String> version = (ArrayList<String>) document1.get(GetStringKey.version);
                                GetDataFirebase.version = version;
                                informationData.putINFDATALIST(arrayListTitile);
                                progressBar.setProgress(33);
//                                    Log.d(LOG, arrayListTitile.get(0));
                            } else {
                                Log.d(LOG, "Error1");
                            }
                        }
                    });
            handler.postDelayed(runnableDownloadTitleBody,Milis);
        }
    };

    static Runnable runnableDownloadTitleBody = new Runnable() {
        @Override
        public void run() {
            txtLoad.setText("Xin chờ để load dữ liệu 2/3");
            final ArrayList<String> arrayListTitile = informationData.getINFDATALIST();
            if (arrayListTitile.size() != 0) {
                final int processss = 33 / arrayListTitile.size();
                for (int z = 0; z < arrayListTitile.size(); z++) {
                    final int finalZ = z;
                    Log.d(LOG, arrayListTitile.get(finalZ));
                    db.collection(arrayListTitile.get(finalZ)).document(GetStringKey.Data)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document2 = task.getResult();
                                        if (document2.exists()) {
                                            ArrayList<String> arrayListTitleHeadBody = (ArrayList<String>) document2.get(GetStringKey.arraylistbody);
                                            ArrayList<ItemInformation> itemInformationArrayList = new ArrayList<>();
                                            Log.d(LOG, "" + arrayListTitleHeadBody.size());
                                            if (type.compareTo("full") == 0) {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            new ArrayList<ItemBody>(), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            } else {
                                                for (int i = 0; i < arrayListTitleHeadBody.size(); i++) {
                                                    itemInformationArrayList.add(new ItemInformation(arrayListTitleHeadBody.get(i), GeneralData.getFavoriteItem(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)),
                                                            GeneralData.getItemBody(activity, arrayListTitile.get(finalZ), arrayListTitleHeadBody.get(i)), arrayListTitile.get(finalZ)));
//                                                Log.d(LOG, arrayListTitleHeadBody.get(i));
                                                }
                                            }
                                            informationData.putInformationData(itemInformationArrayList, arrayListTitile.get(finalZ));
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        } else {
                                            progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                            informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                            Log.d(LOG, "Error2");
                                        }
                                    } else {
                                        progressBar.setProgress(GetDataFirebase.progressBar.getProgress() + processss);
                                        informationData.putInformationData(new ArrayList<ItemInformation>(), arrayListTitile.get(finalZ));
                                        Log.d(LOG, "Error2");
                                    }
                                }
                            });
                }
                if (type.compareTo("full")==0) {
                    handler.postDelayed(runnableDownloadFullBody, Milis);
                }
            }
            else {
                txtLoad.setText("Load dữ liệu 1/3 thất bại");
                handler.postDelayed(runnableDownloadTitle,Milis);
            }
        }
    };
    static Runnable runnableDownloadFullBody = new Runnable() {
        @Override
        public void run() {
            txtLoad.setText("Xin chờ để load dữ liệu 3/3");
            final ArrayList<String> arrayListTitile = informationData.getINFDATALIST();
            int processsssi = 33/arrayListTitile.size();
            for (int i = 0; i < arrayListTitile.size(); i++) {
                final ArrayList<ItemInformation> informationArrayList = informationData.getInformationData(arrayListTitile.get(i));
                int processsssk = processsssi;
                if(informationArrayList.size()!=0)
                {
                    processsssk = processsssi/informationArrayList.size();
                }
                for (int k = 0; k < informationArrayList.size(); k++) {
                    final int finalI = i;
                    final int finalK = k;
                    final int finalProcesssssk = processsssk;
                    db.collection(arrayListTitile.get(i)).document(informationArrayList.get(k).getStrInformation())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document3 = task.getResult();
                                        if(document3.exists()) {
                                            ArrayList<String> arrayListMathView = (ArrayList<String>) document3.get(GetStringKey.arraylistmathview);
                                            ArrayList<String> arrayListTitleBody = (ArrayList<String>) document3.get(GetStringKey.arraystrtitle);
                                            ArrayList<String> arrayimagelist = (ArrayList<String>) document3.get(GetStringKey.arrayimagelist);
                                            String strstt = (String) document3.get(GetStringKey.strstt);
                                            String strlocal = (String) document3.get(GetStringKey.strlocal);
                                            String[] arrayListStt = strstt.split(" ");
                                            String[] arrayListLocal = strlocal.split(" ");
                                            ArrayList<ItemBody> itemBodyArrayList = new ArrayList<>();
                                            int loca1 = 0, loca2 = 0, loca3 = 0;
                                            for (int i = 0; i < arrayListStt.length; i++) {
                                                //1:title 2:mathview 3:grapview
                                                switch (arrayListStt[i]) {
                                                    case "1": {
                                                        itemBodyArrayList.add(new ItemBody(arrayListTitleBody.get(loca1), 1,Integer.parseInt(arrayListLocal[i])));
                                                        loca1++;
                                                    }
                                                    break;
                                                    case "2": {
                                                        itemBodyArrayList.add(new ItemBody(arrayListMathView.get(loca2), 2,Integer.parseInt(arrayListLocal[i])));
                                                        loca2++;
                                                    }
                                                    break;
                                                    case "3": {
                                                        final String image = arrayimagelist.get(loca3).replace("/","");
                                                        StorageReference islandRef = storageRef.child(arrayimagelist.get(loca3));
                                                        final long ONE_MEGABYTE = 1024 * 1024;
                                                        final int finalLoca = loca3;
                                                        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                                            @Override
                                                            public void onSuccess(byte[] bytes) {
                                                                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                                                informationData.putBitmap(bitmap,image);
                                                            }
                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception exception) {
                                                                // Handle any errors
                                                            }
                                                        });
                                                        itemBodyArrayList.add(new ItemBody(image,3,Integer.parseInt(arrayListLocal[i])));
                                                        loca3++;
                                                    }
                                                    break;
                                                }
                                            }
                                            informationArrayList.get(finalK).setItemBodyArrayList(itemBodyArrayList);
                                            informationData.putInformationData(informationArrayList, arrayListTitile.get(finalI));
                                            progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                        }
                                        else
                                        {
                                            progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                            Log.d(LOG, "Error3");
                                        }
                                    } else {
                                        progressBar.setProgress(progressBar.getProgress()+ finalProcesssssk);
                                        Log.d(LOG, "Error3");
                                    }
                                }
                            });
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            handler.postDelayed(runnableStartHome,Milis);
        }
    };
    static Runnable runnableStartHome = new Runnable() {
        @Override
        public void run() {
//            if(progressBar.getProgress() > 90) {
//            progressBar.setProgress(100);
                txtLoad.setText("Load dữ liệu hoàn tất");
                informationData.putKey(GetStringKey.version,""+version.get(version.size()-1));
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
//            }
//            else {
//                txtLoad.setText("Load dữ liệu thất bại");
//            }
        }
    };

    public static boolean isNetworkAvailable(Context con) {
        try {
            ConnectivityManager cm = (ConnectivityManager) con
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
