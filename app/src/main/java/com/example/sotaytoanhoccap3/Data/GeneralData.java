package com.example.sotaytoanhoccap3.Data;

import android.content.Context;
import android.util.Log;

import com.example.sotaytoanhoccap3.Activity.MainActivity;
import com.example.sotaytoanhoccap3.Item.ItemBody;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GeneralData {
    private static InformationData informationData;
    private static int MAXRECENTLY = 500;
    private static ArrayList<String> arrayListTitle;

    public static ArrayList<String> getArrayStrTitle(Context context)
    {
        informationData = new InformationData(context);
        arrayListTitle = informationData.getINFDATALIST();
        return arrayListTitle;
    }

    public static void putArrayStrTitle(Context context,ArrayList<String> arrayListTitle)
    {
        informationData = new InformationData(context);
        GeneralData.arrayListTitle = arrayListTitle;
        informationData.putINFDATALIST(arrayListTitle);
    }

    public static  ArrayList<ItemInformation> getArrayListItemInfo(Context context,String strTitle)
    {
        informationData = new InformationData(context);
        return informationData.getInformationData(strTitle);
    }

    public static void putArrayListItemInfo(Context context,ArrayList<ItemInformation> itemInformations,String strTitle)
    {
        informationData = new InformationData(context);
        informationData.putInformationData(itemInformations,strTitle);
    }

    public static ArrayList<ItemInformation> getListItemFavorite(Context context)
    {
        informationData = new InformationData(context);
        ArrayList<String> itemListInformations = informationData.getINFDATALIST();

        ArrayList<ItemInformation> informationsFavorite = new ArrayList<>();

        for(int i = 0;i<itemListInformations.size();i++)
        {
            for(int j = 0; j < informationData.getInformationData(itemListInformations.get(i)).size(); j++)
            {
                if(informationData.getInformationData(itemListInformations.get(i)).get(j).getFavorite())
                {
                    informationsFavorite.add(informationData.getInformationData(itemListInformations.get(i)).get(j));
                }
            }
        }
        return informationsFavorite;
    }

    public static void setItemFavorite(Context context,ItemInformation itemInformation)
    {
        informationData = new InformationData(context);
        ArrayList<String> itemListInformations = informationData.getINFDATALIST();

        if(MainActivity.selectItemInformation.getStrInformation().compareTo(itemInformation.getStrInformation()) == 0)
        {
            MainActivity.selectItemInformation = itemInformation;
        }

        ArrayList<ItemInformation> informationRecently = informationData.getInformationDataRecently();
        for(int i = 0; i < informationRecently.size();i++)
        {
            if(informationRecently.get(i).getStrInformation().compareTo(itemInformation.getStrInformation())==0)
            {
                informationRecently.get(i).setALL(itemInformation);
                informationData.putInformationDataRecently(informationRecently);
                break;
            }
        }

        for(int i = 0;i<itemListInformations.size();i++)
        {
            if(itemListInformations.get(i).compareTo(itemInformation.getStrTitle())==0)
            {
                ArrayList<ItemInformation> arrayList = informationData.getInformationData(itemListInformations.get(i));
                for(int j = 0; j < arrayList.size();j++)
                {
                    if(arrayList.get(j).getStrInformation().compareTo(itemInformation.getStrInformation())==0)
                    {
                        arrayList.get(j).setALL(itemInformation);
                        informationData.putInformationData(arrayList,itemListInformations.get(i));
                        break;
                    }
                }
            }
        }
    }

    public static void putItemRecently(Context context,ItemInformation itemInformation)
    {
        if(itemInformation.getStrTitle().compareTo("Chưa có dữ liệu") != 0) {
            ArrayList<ItemInformation> itemInformationArrayList;
            InformationData informationData = new InformationData(context);
            if (informationData.getInformationDataRecently() == null) {
                itemInformationArrayList = new ArrayList<>();
                itemInformationArrayList.add(itemInformation);
                Log.d("abc", "true");
            } else {
                Log.d("abc", "false");
                itemInformationArrayList = informationData.getInformationDataRecently();
                if (itemInformationArrayList.size() > 0) {
                    for (int i = 0; i < itemInformationArrayList.size(); i++) {
                        if (itemInformationArrayList.get(i).getStrInformation().compareTo(itemInformation.getStrInformation()) == 0) {
                            itemInformationArrayList.remove(i);
                            break;
                        }
                    }
                }
                itemInformationArrayList.add(itemInformation);
            }
            informationData.putInformationDataRecently(itemInformationArrayList);
            itemInformationArrayList.clear();
            Log.d("abc", "truetruetrue");
            MainActivity.selectItemInformation = itemInformation;
        }
    }

    public static void setPositionStatusRecentlyAndDataRecently(Context context)
    {
        informationData = new InformationData(context);
        if(informationData.getInformationDataRecently()!=null)
        {
            ArrayList<ItemInformation> listRecently = informationData.getInformationDataRecently();
            ArrayList<String> listTitle = informationData.getINFDATALIST();
            if(listTitle!=null)
            {
                for (int i = 0;i<listTitle.size();i++)
                {
                    ArrayList<ItemInformation> arrayList = informationData.getInformationData(listTitle.get(i));
                    if(arrayList!=null)
                    {
                        for(int j = 0; j < arrayList.size();j++)
                        {
                            for(int k = 0;k < listRecently.size();k++)
                            {
                                if(listRecently.get(k).getStrTitle().compareTo(arrayList.get(j).getStrTitle()) == 0)
                                {
                                    if(listRecently.get(k).getStrInformation().compareTo(arrayList.get(j).getStrInformation()) == 0)
                                    {
                                        arrayList.get(j).setPositionStatus(listRecently.get(k).getPositionStatus());
                                        listRecently.get(k).setItemBodyArrayList(arrayList.get(j).getItemBodyArrayList());
                                    }
                                    else {
                                        arrayList.get(j).setPositionStatus(0);
                                    }
                                }
                            }
                        }
                    }
                    informationData.putInformationData(arrayList,listTitle.get(i));
                }
                informationData.putInformationDataRecently(listRecently);
            }
            listRecently.clear();
            listTitle.clear();
        }
    }

    public static void setRecentlyFirebaseToLocal(Context context,String strListRecentLy)
    {
        Gson gson = new Gson();
        Type arrayListTypeTokenItemInfor = new TypeToken<ArrayList<ItemInformation>>(){}.getType();
        informationData = new InformationData(context);
        ArrayList<ItemInformation> listRecently = gson.fromJson(strListRecentLy,arrayListTypeTokenItemInfor);
        informationData.putInformationDataRecently(listRecently);
        setPositionStatusRecentlyAndDataRecently(context);
        listRecently.clear();
    }

    public static void setFavoriteFirebaseToLocal(Context context,String strListFavorite)
    {
        Gson gson = new Gson();
        Type arrayListTypeTokenItemInfor = new TypeToken<ArrayList<ItemInformation>>(){}.getType();
        informationData = new InformationData(context);
        ArrayList<ItemInformation> listFavorite = gson.fromJson(strListFavorite,arrayListTypeTokenItemInfor);

        ArrayList<String> listTitle = informationData.getINFDATALIST();
        if(listTitle!=null) {
            for (int i = 0; i < listTitle.size(); i++) {
                ArrayList<ItemInformation> arrayList = informationData.getInformationData(listTitle.get(i));
                if (arrayList != null) {
                    for (int j = 0; j < arrayList.size(); j++) {
                        for (int k = 0; k < listFavorite.size(); k++) {
                            if (listFavorite.get(k).getStrTitle().compareTo(arrayList.get(j).getStrTitle()) == 0) {
                                if (listFavorite.get(k).getStrInformation().compareTo(arrayList.get(j).getStrInformation()) == 0) {
                                    arrayList.get(j).setFavorite(listFavorite.get(k).getFavorite());
                                } else {
                                    arrayList.get(j).setFavorite(false);
                                }
                            }
                        }
                    }
                }
                informationData.putInformationData(arrayList, listTitle.get(i));
            }
        }
        listTitle.clear();
        listFavorite.clear();
    }

    public static String getTitle(String strTitle)
    {
        int loca = strTitle.indexOf('.');
        if(loca!=-1)
        {
            return new String().copyValueOf(strTitle.toCharArray(),0,loca+1);
        }
        return "";
    }

    public static Boolean getFavoriteItem(Context context, String strTitle,String strTitleBody)
    {
        Boolean check = false;

        informationData = new InformationData(context);
        ArrayList<ItemInformation> informationArrayList = informationData.getInformationData(strTitle);
        if(informationArrayList!=null)
        {
            for(int i = 0; i < informationArrayList.size();i++)
            {
                if(informationArrayList.get(i).getStrInformation().compareTo(strTitleBody) == 0)
                {
                    return informationArrayList.get(i).getFavorite();
                }
            }
        }


        return check;
    }

    public static ArrayList<ItemBody> getItemBody(Context context, String strTitle,String strTitleBody)
    {
        ArrayList<ItemBody> itemBodies = new ArrayList<>();

        informationData = new InformationData(context);
        ArrayList<ItemInformation> informationArrayList = informationData.getInformationData(strTitle);
        if(informationArrayList!=null)
        {
            for(int i = 0; i < informationArrayList.size();i++)
            {
                if(informationArrayList.get(i).getStrInformation().compareTo(strTitleBody) == 0)
                {
                    return informationArrayList.get(i).getItemBodyArrayList();
                }
            }
        }

        return itemBodies;
    }
}
