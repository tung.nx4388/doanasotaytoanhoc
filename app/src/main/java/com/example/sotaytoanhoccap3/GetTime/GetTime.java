package com.example.sotaytoanhoccap3.GetTime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GetTime {
    SimpleDateFormat simpleDateFormat;
    String time;

    public GetTime()
    {
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss");
        time = simpleDateFormat.format(new Date());
    }

    public GetTime(String time)
    {
        this.time = time;
    }

    public String GETFULL()
    {
        return time;
    }

    public String GETDMY()
    {
        return time.split("_")[0];
    }

    public String GETHMS()
    {
        return time.split("_")[1];
    }
}
