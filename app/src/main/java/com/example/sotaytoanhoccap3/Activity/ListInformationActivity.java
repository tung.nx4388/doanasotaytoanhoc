package com.example.sotaytoanhoccap3.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sotaytoanhoccap3.Adapter.ListInformationAdapter;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.skydoves.elasticviews.ElasticImageView;

import java.util.ArrayList;

public class ListInformationActivity extends AppCompatActivity {

    ElasticImageView img_back;
    TextView txt_title;
    ListView listView;
    ArrayList<ItemInformation> itemInformations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_information);

        img_back = findViewById(R.id.img_back_listinformation);
        txt_title = findViewById(R.id.txt_titlelistinformation);
        listView = findViewById(R.id.listViewInformation);

        if(MainActivity.selectItemInformation.getStrTitle().compareTo("Chưa có dữ liệu")!=0)
        {
            InformationData informationData = new InformationData(this);
            itemInformations = informationData.getInformationData(MainActivity.selectItemInformation.getStrTitle());
            ListInformationAdapter listInformationAdapter = new ListInformationAdapter(itemInformations,this);
            listView.setAdapter(listInformationAdapter);
        }

        txt_title.setText(MainActivity.selectItemInformation.getStrTitle());

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_OK);
                finish();
                overridePendingTransition(R.anim.no_animal, R.anim.slide_in_right);
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animal, R.anim.slide_in_right);
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(this);
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            findViewById(R.id.linearListinformation).setBackgroundColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setBackgroundColor();
    }
}
