package com.example.sotaytoanhoccap3.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sotaytoanhoccap3.Adapter.BodyAdapter;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Fragment.ChatBottomSheetFragment;
import com.example.sotaytoanhoccap3.Fragment.FavoriteFragment;
import com.example.sotaytoanhoccap3.Fragment.RecentlyFragment;
import com.example.sotaytoanhoccap3.Item.ItemBody;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.skydoves.elasticviews.ElasticImageView;

import java.util.ArrayList;


public class BodyActivity extends AppCompatActivity {
    ElasticImageView img_back,img_mess,img_favorite,img_menu,img_fullscreen;
    ListView listView;
    LinearLayout linearLayout;
    FrameLayout frameLayout;

    Handler handler;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (listView.getFirstVisiblePosition() != MainActivity.selectItemInformation.getPositionStatus())
            {
                listView.setSelection(MainActivity.selectItemInformation.getPositionStatus());
                handler.postDelayed(runnable,1000);
            }
            else {
                hideprogressbar();
            }
        }
    };

    public static int RESULT_LISTINFORMATION = 111;

    Boolean show_hide;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        img_back = findViewById(R.id.img_back_content);
        img_menu = findViewById(R.id.img_menu_content);
        img_favorite = findViewById(R.id.img_favoriteicon_content);
        img_mess = findViewById(R.id.img_mess_content);
        img_fullscreen = findViewById(R.id.img_fullscreen);
        listView = findViewById(R.id.listContent);
        linearLayout = findViewById(R.id.lin_contenthead);
        frameLayout = findViewById(R.id.framelayout_progress_circular);

        show_hide = true;
        if(MainActivity.selectItemInformation.getFavorite())
        {
            img_favorite.setImageResource(R.mipmap.ic_loveon_foreground);
        }
        else {
            img_favorite.setImageResource(R.mipmap.ic_loveoff_foreground);
        }

        img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show_hide)
                {
                    if(!MainActivity.selectItemInformation.getFavorite())
                    {
                        img_favorite.setImageResource(R.mipmap.ic_loveon_foreground);
                        MainActivity.selectItemInformation.setFavorite(!MainActivity.selectItemInformation.getFavorite());
                        GeneralData.setItemFavorite(MainActivity.activity,MainActivity.selectItemInformation);
                    }
                    else {
                        img_favorite.setImageResource(R.mipmap.ic_loveoff_foreground);
                        MainActivity.selectItemInformation.setFavorite(!MainActivity.selectItemInformation.getFavorite());
                        GeneralData.setItemFavorite(MainActivity.activity,MainActivity.selectItemInformation);
                    }
                    if(GetDataFirebase.isNetworkAvailable(getApplicationContext()))
                    {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        GetDataFirebase.CretateUID(user,db,getApplicationContext());
                    }
                }
            }
        });

        img_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show_hide)
                {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -linearLayout.getHeight());
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    linearLayout.startAnimation(animate);
                    img_fullscreen.setImageResource(R.mipmap.ic_fullscreenexit_foreground);
                    linearLayout.setVisibility(View.GONE);
                    show_hide = !show_hide;
                }
                else {
                    linearLayout.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -linearLayout.getHeight(),
                            0);
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    linearLayout.startAnimation(animate);
                    img_fullscreen.setImageResource(R.mipmap.ic_fullscreen_foreground);
                    show_hide = !show_hide;
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show_hide) {
                    RecentlyFragment.Install(MainActivity.activity);
                    FavoriteFragment.Install(MainActivity.activity);
                    finish();
                    overridePendingTransition(R.anim.no_animal, R.anim.slide_in_bottom);
                }
            }
        });

        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (show_hide) {
                    startActivityForResult(new Intent(BodyActivity.this, ListInformationActivity.class),RESULT_LISTINFORMATION);
                    overridePendingTransition(R.anim.slide_out_right, R.anim.no_animal);
                }
            }
        });

        img_mess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatBottomSheetFragment chatBottomSheetFragment = new ChatBottomSheetFragment();
                chatBottomSheetFragment.show(getSupportFragmentManager(),chatBottomSheetFragment.getTag());
            }
        });

            if(MainActivity.selectItemInformation.getItemBodyArrayList() == null)
            {
                MainActivity.selectItemInformation.setItemBodyArrayList(new ArrayList<ItemBody>());
            }
            else {
                Log.d("abcabc","aaaaaaaa");
            }

            BodyAdapter bodyAdapter = new BodyAdapter(MainActivity.selectItemInformation.getItemBodyArrayList(),MainActivity.activity);
            listView.setAdapter(bodyAdapter);
            if(MainActivity.selectItemInformation.getPositionStatus()!=0)
            {
                showprogressbar();
                handler = new Handler();
                handler.postDelayed(runnable,5000);
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LISTINFORMATION)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                if(MainActivity.selectItemInformation.getFavorite())
                {
                    img_favorite.setImageResource(R.mipmap.ic_loveon_foreground);
                }
                else {
                    img_favorite.setImageResource(R.mipmap.ic_loveoff_foreground);
                }
                if(MainActivity.selectItemInformation.getItemBodyArrayList() == null)
                {
                    MainActivity.selectItemInformation.setItemBodyArrayList(new ArrayList<ItemBody>());
                }
                else {
                    Log.d("abcabc","aaaaaaaa");
                }
                BodyAdapter bodyAdapter = new BodyAdapter(MainActivity.selectItemInformation.getItemBodyArrayList(),MainActivity.activity);
                listView.setAdapter(bodyAdapter);
                if(MainActivity.selectItemInformation.getPositionStatus()!=0)
                {
                    showprogressbar();
                    handler = new Handler();
                    handler.postDelayed(runnable,2000);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        RecentlyFragment.Install(MainActivity.activity);
        FavoriteFragment.Install(MainActivity.activity);
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animal, R.anim.slide_in_bottom);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainActivity.selectItemInformation.setPositionStatus(listView.getFirstVisiblePosition());
        GeneralData.putItemRecently(this,MainActivity.selectItemInformation);
        if(GetDataFirebase.isNetworkAvailable(getApplicationContext()))
        {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            GetDataFirebase.CretateUID(user,db,getApplicationContext());
        }
    }

    private void showprogressbar()
    {
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void hideprogressbar()
    {
        frameLayout.setVisibility(View.INVISIBLE);
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(this);
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            findViewById(R.id.lin_contenthead).setBackgroundColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setBackgroundColor();
    }
}
