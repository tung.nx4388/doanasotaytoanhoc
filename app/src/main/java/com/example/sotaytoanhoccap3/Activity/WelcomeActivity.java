package com.example.sotaytoanhoccap3.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Fragment.UpdateCheckFragment;
import com.example.sotaytoanhoccap3.Interface.CheckUpdateVersion;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class WelcomeActivity extends AppCompatActivity implements CheckUpdateVersion {
    ProgressBar progressBar;
    TextView txtLoad,txt_version;
    ImageView img_logo;
    DialogFragment updateCheckFragment;
    InformationData informationData;
    FirebaseFirestore db;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wecome);

        txtLoad = findViewById(R.id.txtLoading);
        progressBar = findViewById(R.id.progress_horizontal);
        txt_version = findViewById(R.id.txt_versionwelcome);
        img_logo = findViewById(R.id.img_logowelcome);

        db = FirebaseFirestore.getInstance();

        informationData = new InformationData(this);
        if(informationData.getKey(GetStringKey.version).compareTo("")!=0)
        {
            txt_version.setText("Version: "+informationData.getKey(GetStringKey.version));
        }
        else {
            txt_version.setVisibility(View.INVISIBLE);
        }

        if(GetDataFirebase.isNetworkAvailable(this))
        {
            Toast.makeText(this, "Kết nối internet thành công", Toast.LENGTH_SHORT).show();
            if(informationData.getKey(GetStringKey.version).compareTo("")==0)
            {
                CheckUpdateVersion checkUpdateVersion = this;
                checkUpdateVersion.setDowndloadDataFull(true);
            }
            else {
                GetDataFirebase.GetVersion(db,this);
            }
        }
        else {
            Toast.makeText(this, "Không có kết nối internet, ứng dụng sẽ chạy ở chế độ offline", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }

    @Override
    public void setUpdate(Boolean check) {
        if(check)
        {
            txtLoad.setText("Đang tải dữ liệu cập nhật xin vui lòng chờ...");
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            GetDataFirebase.UpdateVersion(db,storageRef,this,txtLoad,progressBar);
            updateCheckFragment.dismiss();
        }
        else {
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }

    @Override
    public void setStVersion(String stVersion) {
        Double versionDB = Double.parseDouble(stVersion);
        Double versionAnd = Double.parseDouble(informationData.getKey(GetStringKey.version));
        if(versionDB>versionAnd)
        {
            updateCheckFragment = new UpdateCheckFragment();
            updateCheckFragment.show(getSupportFragmentManager().beginTransaction().addToBackStack(null),"check");
        }
        else {
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }

    @Override
    public void setDowndloadDataFull(Boolean check) {
        if(check)
        {
            txtLoad.setText("Đang tải dữ liệu đầu tiên xin vui lòng chờ...");
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            GetDataFirebase.GetFullData(db,storageRef,this, txtLoad,progressBar);
        }
    }
}
