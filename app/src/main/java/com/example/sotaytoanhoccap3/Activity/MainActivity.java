package com.example.sotaytoanhoccap3.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.sotaytoanhoccap3.Adapter.MainViewPagerAdapter;
import com.example.sotaytoanhoccap3.Data.GeneralData;
import com.example.sotaytoanhoccap3.Data.GetDataFirebase;
import com.example.sotaytoanhoccap3.Data.InformationData;
import com.example.sotaytoanhoccap3.Fragment.FavoriteFragment;
import com.example.sotaytoanhoccap3.Fragment.LoginDialogFragment;
import com.example.sotaytoanhoccap3.Fragment.RecentlyFragment;
import com.example.sotaytoanhoccap3.Item.ItemInformation;
import com.example.sotaytoanhoccap3.KeyWord.GetStringKey;
import com.example.sotaytoanhoccap3.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;

    DrawerLayout mDrawer;
    Toolbar toolbar;
    NavigationView nvDrawer;
    BottomNavigationView navView;

    // Make sure to be using androidx.appcompat.app.ActionBarDrawerToggle version.
    ActionBarDrawerToggle drawerToggle;

    FragmentPagerAdapter adapterViewPager;
    ViewPager vpPager;
    ImageView img_login;
    public static ItemInformation selectItemInformation;
    public static Activity activity;
    private View viewNavigation;
    public static TextView txt_accountloginnav;
    public static ImageView img_accountloginmav;
    LinearLayout linearDraview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        selectItemInformation = new ItemInformation("Chưa có dữ liệu", false, null, "");
        activity = this;

        navView = findViewById(R.id.nav_view);
        vpPager = findViewById(R.id.fragment);
        // Find our drawer view
        mDrawer = findViewById(R.id.drawer_layout);
        // Find our drawer view
        nvDrawer = findViewById(R.id.nvView);
        img_login = findViewById(R.id.imageView);
        viewNavigation = nvDrawer.getHeaderView(0);
        txt_accountloginnav = viewNavigation.findViewById(R.id.txt_account);
        img_accountloginmav = viewNavigation.findViewById(R.id.img_login);
        linearDraview = viewNavigation.findViewById(R.id.linearDraview);


        navView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        // Setup drawer view
        adapterViewPager = new MainViewPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(final int position) {
                switch (position) {
                    case 0: {
                        navView.setSelectedItemId(R.id.navigation_home);
                    }
                    break;
                    case 1: {
                        navView.setSelectedItemId(R.id.navigation_recently);
                    }
                    break;
                    case 2: {
                        navView.setSelectedItemId(R.id.navigation_favorite);
                    }
                    break;
                    case 3: {
                        navView.setSelectedItemId(R.id.navigation_setting);
                    }
                    break;
                }
            }


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        setupDrawerContent(nvDrawer);
        img_login.setOnClickListener(this);
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId())
                    {
                        case R.id.navigation_recently:
                            {
                                vpPager.setCurrentItem(1);
                                RecentlyFragment.Install(getBaseContext());
                                return true;
                            }
                        case R.id.navigation_favorite: {
                            vpPager.setCurrentItem(2);
                            FavoriteFragment.Install(getBaseContext());
                            return true;
                        }
                        case R.id.navigation_home:
                        {
                            vpPager.setCurrentItem(0);
                            return true;
                        }
                        case R.id.navigation_setting:
                            {
                                vpPager.setCurrentItem(3);
                                return true;
                            }
                    }
                    return false;
                }
            };

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                fragmentClass = LoginDialogFragment.class;
                break;
            default:
                fragmentClass = LoginDialogFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        DialogFragment dialogFragment = new LoginDialogFragment();
        if(GetDataFirebase.isNetworkAvailable(this)) {
            dialogFragment.show(getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null), "Hello");
        }else {
            Toast.makeText(activity, "Vui lòng kết nối internet để đăng nhập", Toast.LENGTH_SHORT).show();
        }
        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);

        // Set action bar title
        setTitle(menuItem.getTitle());

        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imageView:
            {
                mDrawer.openDrawer(GravityCompat.START);
            }break;
        }
    }

    private void setBackgroundColor()
    {
        InformationData informationData = new InformationData(this);
        if(informationData.getKey(GetStringKey.color).compareTo("")!=0)
        {
            img_login.setColorFilter(Color.parseColor(informationData.getKey(GetStringKey.color)));
            TextView textView = findViewById(R.id.textView);
            textView.setTextColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            ColorStateList iconsColorStates = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{
                            Color.parseColor(informationData.getKey(GetStringKey.colorhint)),
                            Color.parseColor(informationData.getKey(GetStringKey.color))
                    });

            ColorStateList textColorStates = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{
                            Color.parseColor(informationData.getKey(GetStringKey.color)),
                            Color.parseColor(informationData.getKey(GetStringKey.color))
                    });

            navView.setItemTextColor(iconsColorStates);
            navView.setItemIconTintList(iconsColorStates);
            linearDraview.setBackgroundColor(Color.parseColor(informationData.getKey(GetStringKey.color)));
            nvDrawer.setItemTextColor(textColorStates);
        }
        if(informationData.getKey(GetStringKey.choicetheme).compareTo("")!=0)
        {
            switch (informationData.getKey(GetStringKey.choicetheme))
            {
                case "0":{
                    if(informationData.getKey(GetStringKey.themenormal).compareTo("")!=0) {
                        vpPager.setBackgroundResource(Integer.parseInt(informationData.getKey(GetStringKey.themenormal)));
                    }
                }break;
                case "1":{
                    if(informationData.getKey(GetStringKey.themebitmap).compareTo("")!=0) {
                        BitmapDrawable ob = new BitmapDrawable(getResources(), informationData.loadImageFromStorage(GetStringKey.themebitmap));
                        vpPager.setBackground(ob);
                    }
                }break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser!=null)
        {
            txt_accountloginnav.setText(currentUser.getEmail());
        }
        else {
            txt_accountloginnav.setText("No user");
        }

        GeneralData.setPositionStatusRecentlyAndDataRecently(this);
        setBackgroundColor();
        if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
            // do your stuff..
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[] { permission },
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // do your stuff
                } else {
                    Toast.makeText(this, "GET_ACCOUNTS Denied",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
        }
    }
}
